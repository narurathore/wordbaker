package com.blazestars.wordbaker.Activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.blazestars.wordbaker.Helper.AnalyticsHelper;
import com.blazestars.wordbaker.R;

public class HowToPlayActivity extends AppCompatActivity {

    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_how_to_play);
        mContext = this;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView titleTV = (TextView) toolbar.findViewById(R.id.title);
        setSupportActionBar(toolbar);
        titleTV.setText("How To Play");
        try {
            String pageviews = "How to play";
            AnalyticsHelper.trackPageView(pageviews, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    public void continueClicked(View v){
        finish();
    }

    public void onNextClicked(View v){
        findViewById(R.id.how_to_play_game_screen_layout).setVisibility(View.GONE);
        findViewById(R.id.how_to_play_word_layout).setVisibility(View.VISIBLE);
    }
}
