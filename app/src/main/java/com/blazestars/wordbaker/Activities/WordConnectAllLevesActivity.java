package com.blazestars.wordbaker.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.blazestars.wordbaker.Adapters.StageAdapter;
import com.blazestars.wordbaker.Adapters.WordConnectAllLevelActivityAdapter;
import com.blazestars.wordbaker.Helper.APICallHelper;
import com.blazestars.wordbaker.Helper.AnalyticsHelper;
import com.blazestars.wordbaker.Modal.GameData;
import com.blazestars.wordbaker.Modal.NameConstant;
import com.blazestars.wordbaker.Modal.Stage;
import com.blazestars.wordbaker.Modal.WordConnectLevel;
import com.blazestars.wordbaker.Network.VolleyData;
import com.blazestars.wordbaker.R;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;

public class WordConnectAllLevesActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    WordConnectAllLevelActivityAdapter stageAdapter;
    Activity mContext;
    GameData gameData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stage);
        gameData = new APICallHelper().getGameDataCached(this);
        if (gameData == null){
            finish();
        }
        mContext = this;
        initiateUIComponents();
        try {
            String pageviews = "Word Connect Level Screen";
            AnalyticsHelper.trackPageView(pageviews, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    WordConnectAllLevelActivityAdapter.OnItemClickListener stageItemClickListener = new WordConnectAllLevelActivityAdapter.OnItemClickListener() {
        @Override
        public void onItemClick(View view, int position) {
            try {
                String categoryName = "Level";
                String label = "" + gameData.getWordConnectLevels().get(position).getLevelNo();
                String action = "Level Reopened";
                AnalyticsHelper.trackEvent(categoryName, action, label, mContext);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Intent intent = new Intent();
            intent.putExtra("position", position);
            setResult(RESULT_OK, intent);
            finish();
        }
    };

    private void initiateUIComponents(){
        recyclerView = findViewById(R.id.stage_recycler_view);
        if (gameData != null) {
            stageAdapter = new WordConnectAllLevelActivityAdapter(this, gameData.getWordConnectLevels());
        }else {
            finish();
        }
        stageAdapter.setOnItemClickListener(stageItemClickListener);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(stageAdapter);
        TextView diamondCountTv = (TextView) findViewById(R.id.diamond_count_tv);
        SharedPreferences preferences = getSharedPreferences(NameConstant.GAME_DATABASE, MODE_PRIVATE);
        //tempEmptyLetterLayout = findViewById(R.id.temp_empty_letter_layout);
        int diamondCount = preferences.getInt("guest_score",0);
        diamondCountTv.setText(String.valueOf(diamondCount));
    }


    @Override
    protected void onResume() {
        super.onResume();
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    public void onHomeClick(View v){
        Intent intent = new Intent();
        intent.putExtra("isHomeClicked", true);
        setResult(RESULT_OK, intent);
        finish();
        try {
            String categoryName = "Button";
            String label = "Level list";
            String action = "Home Clicked";
            AnalyticsHelper.trackEvent(categoryName, action, label, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
