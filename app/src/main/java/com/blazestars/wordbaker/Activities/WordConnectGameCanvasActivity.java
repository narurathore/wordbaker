package com.blazestars.wordbaker.Activities;

import android.animation.Animator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.adcolony.sdk.AdColony;
import com.adcolony.sdk.AdColonyAdOptions;
import com.adcolony.sdk.AdColonyAppOptions;
import com.adcolony.sdk.AdColonyInterstitial;
import com.adcolony.sdk.AdColonyInterstitialListener;
import com.adcolony.sdk.AdColonyReward;
import com.adcolony.sdk.AdColonyRewardListener;
import com.adcolony.sdk.AdColonyZone;
import com.blazestars.wordbaker.CustomViews.AlertDialogCustom;
import com.blazestars.wordbaker.CustomViews.CanvasView;
import com.blazestars.wordbaker.Helper.APICallHelper;
import com.blazestars.wordbaker.Helper.AnalyticsHelper;
import com.blazestars.wordbaker.Helper.Helper;
import com.blazestars.wordbaker.Helper.MyInterstitialAd;
import com.blazestars.wordbaker.Interfaces.GameScreenListner;
import com.blazestars.wordbaker.Modal.GameData;
import com.blazestars.wordbaker.Modal.NameConstant;
import com.blazestars.wordbaker.Modal.WordConnectLevel;
import com.blazestars.wordbaker.Modal.WordConnectLevelLetter;
import com.blazestars.wordbaker.R;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.AdListener;
import com.facebook.ads.InterstitialAd;
import com.facebook.ads.InterstitialAdListener;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.jar.Attributes;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.MINUTES;

public class WordConnectGameCanvasActivity extends AppCompatActivity implements GameScreenListner, RewardedVideoAdListener {
    List<View> views = new ArrayList<>();
    RelativeLayout canvasButtonContainerLayout;
    final private String APP_ID = "app987457e05af34ec194";
    final private String ZONE_ID = "vza620d2c35cc748a897";
    final private int ONE_MORE_SPIN = 1;
    final private int FIFTY_DIAMONDS = 2;
    final private int TWO_HINTS = 3;
    final private int FORTY_DIAMONDS = 4;
    final private int WORD_HINT = 5;
    final private int THIRTY_DIAMONDS = 6;
    final private int ONE_HINT = 7;
    final private int TWENTY_DIAMONDS = 8;

    private AdColonyAdOptions ad_options;
    private AdColonyInterstitialListener listener;
    public AdColonyInterstitial adcolonyAd;
    RelativeLayout emptyPuzzleLayout;
    CanvasView canvasView;
    WordConnectLevel currentLevel;
    TextView lastTriedWord;
    AlertDialogCustom alertDialogCustom;
    TextView diamondCountTv;
    View actualDiamond;
    MediaPlayer mpMusic;
    private RewardedVideoAd mAd;
    //BitmapDrawable letter_background;
    int SCREEN_WIDTH;
    View diamondLayout;
    TextView toast;
    TextView topToast;
    boolean isDailyPuzzle;
    boolean isDailyPuzzleWasAlreadyCompleted;
    int loadLevelIndex = -1;
    LinearLayout adViewContainer;
    Uri letterBackgroundUri;
    View spinLayout;
    View spinWheel;
    View nife;
    View closeButtonSpinWheel;
    View claimButtonSpinWheel;
    TextView spinTextView;
    View spinIconButton;
    boolean isWheelSpinnig = false;
    int spinResult = 0;
    public MyInterstitialAd myInterstitialAd;
    boolean wasCurrentLevelNull = false;
    Handler handler = new Handler();
    GameData gameData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_word_connect);
        gameData = new APICallHelper().getGameDataCached(this);
        if (gameData == null){
            finish();
        }
        initViews();
        //setTimerForAds();
        loadAndInitiateAds(gameData.getBannerAdPriority());
        mAd = MobileAds.getRewardedVideoAdInstance(this);
        mAd.setRewardedVideoAdListener(this);
        initAdcolony();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                loadNewStage();
            }
        }, 200);
    }

    private void initViews(){
        isDailyPuzzle = getIntent().getBooleanExtra(NameConstant.INTENT_DATA_KEY_IS_DAILY_PUZZLE,
                false);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        SCREEN_WIDTH = displayMetrics.widthPixels;
        //letter_background = new BitmapDrawable(getResources(), BitmapFactory.decodeResource(
        //        getResources(),R.drawable.letter_background));
        letterBackgroundUri = Helper.getURIForResourceImage(R.drawable.letter_background);
        canvasView = findViewById(R.id.canvas_view);
        diamondLayout = findViewById(R.id.diamond_layout);
        canvasButtonContainerLayout = findViewById(R.id.canvas_button_container_layout);
        emptyPuzzleLayout = findViewById(R.id.empty_puzzle_layout);
        canvasView.setGameListner(this);
        lastTriedWord = (TextView) findViewById(R.id.lastTriedWord);
        alertDialogCustom = (AlertDialogCustom) findViewById(R.id.alert_dialog_reward);
        actualDiamond = findViewById(R.id.actual_diamond);
        adViewContainer = findViewById(R.id.adView);
        diamondCountTv = (TextView) findViewById(R.id.diamond_count_tv);
        toast = findViewById(R.id.toast);
        topToast = findViewById(R.id.top_toast);
        spinLayout = findViewById(R.id.spin_wheel_container_layout);
        spinWheel = findViewById(R.id.spin_wheel_image_view);
        nife = findViewById(R.id.nife_image_view);
        spinTextView = findViewById(R.id.spin_text);
        closeButtonSpinWheel = findViewById(R.id.close_button);
        claimButtonSpinWheel = findViewById(R.id.claim_button);
        spinIconButton = findViewById(R.id.spin_icon_button);
        SharedPreferences preferences = getSharedPreferences(NameConstant.GAME_DATABASE, MODE_PRIVATE);
        //tempEmptyLetterLayout = findViewById(R.id.temp_empty_letter_layout);
        int diamondCount = preferences.getInt("guest_score",0);
        diamondCountTv.setText(String.valueOf(diamondCount));
        ((SimpleDraweeView)findViewById(R.id.my_image_view)).setImageURI(Helper.
                getURIForResourceImage(Helper.getResourceOfBackGround()));
        ((SimpleDraweeView)findViewById(R.id.actual_diamond)).setImageURI(Helper.
                getURIForResourceImage(R.drawable.diamond));
        ((SimpleDraweeView)findViewById(R.id.plus_button)).setImageURI(Helper.
                getURIForResourceImage(R.drawable.plus));
        ((SimpleDraweeView)findViewById(R.id.share_level_button)).setImageURI(Helper.
                getURIForResourceImage(R.drawable.share_level_button));
        ((SimpleDraweeView)findViewById(R.id.level_list_button)).setImageURI(Helper.
                getURIForResourceImage(R.drawable.level_list_button));
        ((SimpleDraweeView)findViewById(R.id.shuffle_button)).setImageURI(Helper.
                getURIForResourceImage(R.drawable.shuffle_button));
        ((SimpleDraweeView)findViewById(R.id.hint_button)).setImageURI(Helper.
                getURIForResourceImage(R.drawable.hint_button));
        ((SimpleDraweeView)findViewById(R.id.home_button)).setImageURI(Helper.
                getURIForResourceImage(R.drawable.home_icon));
        ((SimpleDraweeView)findViewById(R.id.nife_image_view)).setImageURI(Helper.
                getURIForResourceImage(R.drawable.nife));
        ((SimpleDraweeView)findViewById(R.id.spin_wheel_image_view)).setImageURI(Helper.
                getURIForResourceImage(R.drawable.spin_wheel));
        ((SimpleDraweeView)findViewById(R.id.close_button)).setImageURI(Helper.
                getURIForResourceImage(R.drawable.close_button));
        ((SimpleDraweeView)findViewById(R.id.claim_button)).setImageURI(Helper.
                getURIForResourceImage(R.drawable.claim_button));
        ((SimpleDraweeView)findViewById(R.id.spin_icon_button)).setImageURI(Helper.
                getURIForResourceImage(R.drawable.spin_wheel_icon));
        mpMusic = MediaPlayer.create(this, R.raw.game_music);
        if (Helper.isSoundEnabled(this)) {

            if (mpMusic != null) {
                mpMusic.setVolume(0.25f, 0.25f);
                mpMusic.setLooping(true);
                mpMusic.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        if (mp != null)
                            mp.release();
                    }
                });
                mpMusic.start();
            }
        }
        if (myInterstitialAd == null){
        //    myInterstitialAd = new MyInterstitialAd(this, false);
        }
    }

    private void fillAllLettersOfPuzzle(){
        if (currentLevel != null) {
            for (WordConnectLevelLetter letter : currentLevel.getPuzzleLetters()){
                showLetter(letter);
            }
        }
    }

    @Override
    public void loadNewStage() {
        if (canvasButtonContainerLayout != null && gameData != null) {
            //add letter layouts in matrix form
            if (currentLevel == null){
                wasCurrentLevelNull = true;
            }
            if (loadLevelIndex != -1 && gameData.getWordConnectLevels() != null
                    && gameData.getWordConnectLevels().size() > loadLevelIndex){
                currentLevel = gameData.getWordConnectLevels().get(loadLevelIndex);
                currentLevel.getWordsFormed().clear();
                for (WordConnectLevelLetter letter : currentLevel.getPuzzleLetters()){
                    letter.setLetterFilled(false);
                }
                isDailyPuzzle = false;
            }else {
                if (currentLevel != null && isDailyPuzzle){
                    return;
                }
                if (isDailyPuzzle) {
                    currentLevel = gameData.getDailyPuzzle();
                    if (currentLevel == null) {
                        toast.setVisibility(View.VISIBLE);
                        toast.setText("There is no puzzle for you today. Please come back tomorrow");
                        canvasView.setVisibility(View.GONE);
                        return;
                    }
                } else {
                    currentLevel = gameData.getLastUnfinishedWordConnectLevel();
                }
            }
            if (currentLevel == null){
                toast.setVisibility(View.VISIBLE);
                toast.setText("Congratulations! You have completed the Game");
                canvasView.setVisibility(View.GONE);
                try {
                    String categoryName = "Game";
                    String label = String.valueOf(currentLevel.getLevelNo());
                    String action = "Completed";
                    AnalyticsHelper.trackEvent(categoryName, action, label, this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return;
            }

            if (currentLevel.getLevelNo() == 1 || currentLevel.getLevelNo() % 4 == 0) {
                ((SimpleDraweeView)findViewById(R.id.my_image_view)).setImageURI(Helper.
                        getURIForResourceImage(Helper.getResourceOfBackGround()));
            }
            final TextView titleTV = (TextView) findViewById(R.id.level_name);

            try {
                String categoryName = "Level";
                String label = String.valueOf(currentLevel.getLevelNo());
                String action = "Loaded";
                AnalyticsHelper.trackEvent(categoryName, action, label, this);
            } catch (Exception e) {
                e.printStackTrace();
            }

            titleTV.setText("Level: " + currentLevel.getLevelNo());
            canvasButtonContainerLayout.removeAllViews();
            canvasView.resetCanvas();
            lastTriedWord.setVisibility(View.GONE);
            addEmptyLetterPuzzle();
            canvasView.setCurrentLevel(currentLevel);
            canvasView.setHand(findViewById(R.id.hand));
            for (int i = 0; i < currentLevel.getCanvasLetters().length(); i++) {
                    final View letterLayout = getLayoutInflater().inflate(R.layout.letter_layout, null);
                    RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(
                            RelativeLayout.LayoutParams.WRAP_CONTENT,
                            RelativeLayout.LayoutParams.WRAP_CONTENT);
                    View letterInnerLayout = letterLayout.findViewById(R.id.letter_layout);
                    //button.setImageResource(Helper.getLetterImageResID((currentLevel.getCanvasLetters().charAt(i))));
                    letterInnerLayout.setTag(currentLevel.getCanvasLetters().charAt(i));
                    //letterInnerLayout.setBackground(letter_background);
                ((SimpleDraweeView)letterLayout.findViewById(R.id.image_view_letter)).setImageURI(letterBackgroundUri);
                TextView letterTextView = letterLayout.findViewById(R.id.letter_text);
                letterTextView.setText(String.valueOf(currentLevel.getCanvasLetters().charAt(i)));
                    letterTextView.setTag(-1);
                    letterLayout.setLayoutParams(rlp);
                    canvasButtonContainerLayout.addView(letterLayout);
                    views.add(letterLayout);
            }
            canvasView.setParentViewOfButtons(canvasButtonContainerLayout);
            canvasView.setLettersInCanvas(views);
            canvasView.arrangeButtons();
            if (isDailyPuzzle){
                titleTV.setText("Today's Puzzle");
            }
            if (currentLevel.isCompleted() && isDailyPuzzle){
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        isDailyPuzzleWasAlreadyCompleted = true;
                        fillAllLettersOfPuzzle();
                        toast.setVisibility(View.VISIBLE);
                        toast.setText("You have already completed today's puzzle.\nCome back tomorrow to play again.");
                        canvasView.setVisibility(View.GONE);
                    }
                }, 200);

                return;
            }
            if (loadLevelIndex == -1) {
                currentLevel.clearLevelToPlayAgain(this, isDailyPuzzle);
            }else {
                loadLevelIndex++;
            }
            preFillLastState();
            showSpinWheelIcon();
        }
        System.gc();
    }

    Runnable bannerAdRunnable = new Runnable() {
        @Override
        public void run() {
            loadAndInitiateAds(gameData.getBannerAdPriority());
        }
    };

    private void preFillLastState(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (currentLevel != null && currentLevel.getWordsFormed() != null
                        && currentLevel.getWordsFormed().size() > 0){
                    for (String word : currentLevel.getWordsFormed()) {
                        List<WordConnectLevelLetter> letters = currentLevel.getLettersOfWordsFormed(word);
                        for (WordConnectLevelLetter letter : letters){
                            showLetter(letter);
                        }
                    }
                }
            }
        }, 200);

    }

    private void addEmptyLetterPuzzle(){
        emptyPuzzleLayout.removeAllViews();
        if (currentLevel == null){
            return;
        }
        WordConnectLevelLetter letter;
        int drawableWidth = SCREEN_WIDTH - 400;
        if (currentLevel.getXOrYWidthMax() > 8){
            drawableWidth = SCREEN_WIDTH - 200;
        }
        int emptyLetterWidth = drawableWidth / currentLevel.getXOrYWidthMax();
        RelativeLayout.LayoutParams lpEmptyLayoutParams = new RelativeLayout.LayoutParams(drawableWidth + currentLevel.getXMax()*3, drawableWidth);
        LinearLayout.LayoutParams lpParams = new LinearLayout.LayoutParams(emptyLetterWidth, emptyLetterWidth);
        //tempEmptyLetterLayout.setLayoutParams(lpEmptyLayoutParams);
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setLayoutParams(lpEmptyLayoutParams);
        emptyPuzzleLayout.addView(linearLayout);
        int diffInXAndY = currentLevel.getXOrYWidthMax() - currentLevel.getXMax();
        for (int i = 0; i < currentLevel.getPuzzleLetters().size(); i++){
            letter = currentLevel.getPuzzleLetters().get(i);
            View letterLayout = getLayoutInflater().inflate(R.layout.empty_letter_layout_for_word_connect, null);
            letterLayout.setTag("" + letter.getX() +"_"+ letter.getY());
            emptyPuzzleLayout.addView(letterLayout);
            RelativeLayout innerLinearLayout = letterLayout.findViewById(R.id.letter_layout);
            innerLinearLayout.setLayoutParams(lpParams);
            letterLayout.setX(emptyLetterWidth * letter.getX() + (letter.getX() * 3) + (diffInXAndY * emptyLetterWidth)/2);
            letterLayout.setY(emptyLetterWidth * letter.getY() + letter.getY() * 3);
        }
    }

    @Override
    public void loadVideo() {
        loadRewardedVideoAd();
    }

    @Override
    public void animateDiamond(final int diamondEarned) {
        actualDiamond.animate()
                .scaleXBy(1)
                .scaleYBy(1)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        actualDiamond.animate()
                                .scaleXBy(-1)
                                .scaleYBy(-1)
                                .setListener(null)
                                .setDuration(300);
                        updateDiamondCount(diamondEarned);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .setDuration(300);
    }

    @Override
    public void wordFormedInCanvas(String word) {
        final Activity activity = this;
        if (!TextUtils.isEmpty(word) && word.length() == 1){
            lastTriedWord.setBackgroundResource(R.drawable.wrong_word_round_background);
            canvasView.updateStatusAfterWordFormed(NameConstant.WRONG_WORD_FORMED);
            showTopToast("Swipe Letters to form word");
        }else if (currentLevel.isWordAlreadyFormed(word)){
            showToast("Word already formed");
            lastTriedWord.setBackgroundResource(R.drawable.already_formed_word_round_background);
            canvasView.updateStatusAfterWordFormed(NameConstant.WORD_ALREADY_FORMED);
            Helper.playGameSound(this, R.raw.wrong_word_sound);
            try {
                String categoryName = "Word already formed";
                String label = "" + currentLevel.getLevelNo();
                String action = word;
                AnalyticsHelper.trackEvent(categoryName, action, label, this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else if (currentLevel.isExtraWordFormed(word)){
            if (currentLevel.isExtraWordAlreadyFormed(word)){
                showToast("Extra word already formed");
                lastTriedWord.setBackgroundResource(R.drawable.already_formed_word_round_background);
                canvasView.updateStatusAfterWordFormed(NameConstant.WORD_ALREADY_FORMED);
                try {
                    String categoryName = "Extra word already formed";
                    String label = "" + currentLevel.getLevelNo();
                    String action = word;
                    AnalyticsHelper.trackEvent(categoryName, action, label, this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Helper.playGameSound(this, R.raw.wrong_word_sound);
            }else {
                lastTriedWord.setBackgroundResource(R.drawable.correct_word_round_background);
                canvasView.updateStatusAfterWordFormed(NameConstant.COREECT_WORD_FORMED);
                try {
                    String categoryName = "Extra word formed";
                    String label = "" + currentLevel.getLevelNo();
                    String action = word;
                    AnalyticsHelper.trackEvent(categoryName, action, label, this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                currentLevel.addExtraWordFormed(word);
                animateDiamond(currentLevel.getWordPoint());
                final float x = lastTriedWord.getX();
                final float y = lastTriedWord.getY();

                lastTriedWord.animate()
                        .x(diamondLayout.getX())
                        .y(diamondLayout.getY())
                        .setDuration(500)
                        .setListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animator) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animator) {
                                lastTriedWord.setX(x);
                                lastTriedWord.setY(y);
                                showToast("Extra word formed");
                                Helper.playGameSound(activity, R.raw.word_creation_sound);
                            }

                            @Override
                            public void onAnimationCancel(Animator animator) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animator) {

                            }
                        })
                        .start();
            }
        }else if (currentLevel.isWordFormed(word)){
            lastTriedWord.setBackgroundResource(R.drawable.correct_word_round_background);
            canvasView.updateStatusAfterWordFormed(NameConstant.COREECT_WORD_FORMED);
            try {
                String categoryName = "Word formed";
                String label = "" + currentLevel.getLevelNo();
                String action = word;
                AnalyticsHelper.trackEvent(categoryName, action, label, this);
            } catch (Exception e) {
                e.printStackTrace();
            }
            showWordFormed(word);
        } else {
            lastTriedWord.setBackgroundResource(R.drawable.wrong_word_round_background);
            canvasView.updateStatusAfterWordFormed(NameConstant.WRONG_WORD_FORMED);
            try {
                String categoryName = "Wrong word formed";
                String label = "" + currentLevel.getLevelNo();
                String action = word;
                AnalyticsHelper.trackEvent(categoryName, action, label, this);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Helper.playGameSound(this, R.raw.wrong_word_sound);
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                lastTriedWord.setBackgroundResource(R.drawable.basic_word_round_background);
            }
        },500);
    }

    public void onHomeClick(View v){
        finish();
        try {
            String categoryName = "Button";
            String label = "" + currentLevel.getLevelNo();
            String action = "Home Clicked";
            AnalyticsHelper.trackEvent(categoryName, action, label, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideNavigationBar(){
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            hideNavigationBar();
            if (adcolonyAd == null || adcolonyAd.isExpired()) {
                if (listener != null && ad_options != null) {
                    AdColony.requestInterstitial(ZONE_ID, listener, ad_options);
                }
            }
            if (mpMusic != null)
                mpMusic.start();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        try {
            super.onPause();
            if (mpMusic != null)
                mpMusic.pause();

        }catch (Exception ex) {

        }

    }

    @Override
    public void onLetterSelectedInCanvas(String word) {
        if (Helper.isSoundEnabled(this)) {
            final MediaPlayer mp = MediaPlayer.create(this, R.raw.letter_selection_sound);
            if (mp != null) {
                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        if (mp != null) {
                            mp.release();
                        }
                    }
                });
                mp.start();
            }
        }
        lastTriedWord.setVisibility(View.VISIBLE);
        lastTriedWord.setText(word);
    }

    private void updateDiamondCount(int diamondEarned){
        //TODO play sound
        SharedPreferences preferences = getSharedPreferences(NameConstant.GAME_DATABASE, MODE_PRIVATE);
        SharedPreferences.Editor editor = getSharedPreferences(NameConstant.GAME_DATABASE, MODE_PRIVATE).edit();
        int score = preferences.getInt("guest_score",0);
        score = score + diamondEarned;
        editor.putInt("guest_score",score);
        editor.apply();
        Helper.playGameSound(this, R.raw.diamond_earned_sound);
        diamondCountTv.setText(String.valueOf(score));

    }



    private void loadAndInitiateAds(int priority){
        try {
            adViewContainer.removeAllViews();
            if (priority == NameConstant.FACEBOOK_AD){
                com.facebook.ads.AdView adView = new com.facebook.ads.AdView(this,
                        "219755418737678_219756978737522", com.facebook.ads.AdSize.BANNER_HEIGHT_50);
                // Add the ad view to your activity layout
                adView.setAdListener(new AdListener() {
                    @Override
                    public void onError(Ad ad, AdError adError) {
                        loadAndInitiateAds(2);
                    }

                    @Override
                    public void onAdLoaded(Ad ad) {

                    }

                    @Override
                    public void onAdClicked(Ad ad) {

                    }

                    @Override
                    public void onLoggingImpression(Ad ad) {
                        handler.postDelayed(bannerAdRunnable,
                                gameData.getRefershFacebookBannnerAdAfterSecs() * 1000);
                    }
                });
                adViewContainer.addView(adView);
                // Request an ad
                adView.loadAd();
            }else {
                if (MainActivity.adView != null) {
                    adViewContainer.addView(MainActivity.adView);
                }else {
                    AdView adView = new AdView(this);
                    adView.setAdSize(AdSize.SMART_BANNER);
                    adView.setAdUnitId("ca-app-pub-2649338194133294/1570880747");
                    AdRequest adRequest = new AdRequest.Builder().build();
                    adView.loadAd(adRequest);
                    adViewContainer.addView(adView);
                    MainActivity.adView = adView;
                }
            }
        }catch (Exception ex){

        }

    }

    private void initAdcolony(){
        /** Construct optional app options object to be sent with configure */
        AdColonyAppOptions app_options = new AdColonyAppOptions();

        /**
         * Configure AdColony in your launching Activity's onCreate() method so that cached ads can
         * be available as soon as possible.
         */
        AdColony.configure( this, app_options, APP_ID, ZONE_ID );

        /** Ad specific options to be sent with request */
        ad_options = new AdColonyAdOptions()
                .enableConfirmationDialog( false )
                .enableResultsDialog( false );

        /** Create and set a reward listener */
        final Activity activity = this;
        AdColony.setRewardListener( new AdColonyRewardListener()
        {
            @Override
            public void onReward( AdColonyReward reward )
            {
                /** Query reward object for info here */
                //Log.d( TAG, "onReward" );
                /*try {
                    String categoryName = "Hint";
                    String label = "From_Video_" + stage.getName() + "_" + currentLevel.getName()+ "_adColony";
                    String action = "Claimed";
                    AnalyticsHelper.trackEvent(categoryName, action, label, mActivity);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    String categoryName = "Video";
                    String label = stage.getName() + "_" + currentLevel.getName();
                    String action = null;
                    action = "Completed_From_Video_Button_adColony";
                    AnalyticsHelper.trackEvent(categoryName, action, label, mActivity);
                } catch (Exception e) {
                    e.printStackTrace();
                }*/
                alertDialogCustom.setResult(activity, 60);
                alertDialogCustom.showWithAnimation();
            }
        } );

        /**
         * Set up listener for interstitial ad callbacks. You only need to implement the callbacks
         * that you care about. The only required callback is onRequestFilled, as this is the only
         * way to get an ad object.
         */
        listener = new AdColonyInterstitialListener()
        {
            /** Ad passed back in request filled callback, ad can now be shown */
            @Override
            public void onRequestFilled( AdColonyInterstitial ad )
            {
                adcolonyAd = ad;
                //Log.d( TAG, "onRequestFilled" );
            }

            /** Ad request was not filled */
            @Override
            public void onRequestNotFilled( AdColonyZone zone )
            {
                //Log.d( TAG, "onRequestNotFilled");
            }

            /** Ad opened, reset UI to reflect state change */
            @Override
            public void onOpened( AdColonyInterstitial ad )
            {
                //Log.d( TAG, "onOpened" );
                try {
                    String categoryName = "Video";
                    String label = "" +currentLevel.getLevelNo();
                    String action = null;
                    action = "adColony video opened";
                    AnalyticsHelper.trackEvent(categoryName, action, label, activity);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            /** Request a new ad if ad is expiring */
            @Override
            public void onExpiring( AdColonyInterstitial ad )
            {
                AdColony.requestInterstitial( ZONE_ID, this, ad_options );
                //Log.d( TAG, "onExpiring" );
            }
        };
    }

    private void loadRewardedVideoAd() {
        if (true){
            final InterstitialAd interstitialAd = new InterstitialAd(this, "219755418737678_228689094510977");
            // Set listeners for the Interstitial Ad
            interstitialAd.setAdListener(new InterstitialAdListener() {
                @Override
                public void onInterstitialDisplayed(Ad ad) {
                    hideProgressDialog();
                    // Interstitial ad displayed callback
                    Log.e("facebook video ad", "Interstitial ad displayed.");
                }

                @Override
                public void onInterstitialDismissed(Ad ad) {
                    // Interstitial dismissed callback
                    hideProgressDialog();
                    Log.e("facebook video ad", "Interstitial ad dismissed.");
                }

                @Override
                public void onError(Ad ad, AdError adError) {
                    // Ad error callback
                    Log.e("facebook video ad", "Interstitial ad failed to load: " + adError.getErrorMessage());
                }

                @Override
                public void onAdLoaded(Ad ad) {
                    hideProgressDialog();
                    // Interstitial ad is loaded and ready to be displayed
                    Log.d("facebook video ad", "Interstitial ad is loaded and ready to be displayed!");
                    // Show the ad
                    interstitialAd.show();
                }

                @Override
                public void onAdClicked(Ad ad) {
                    // Ad clicked callback
                    Log.d("facebook video ad", "Interstitial ad clicked!");
                }

                @Override
                public void onLoggingImpression(Ad ad) {
                    // Ad impression logged callback
                    Log.d("facebook video ad", "Interstitial ad impression logged!");
                }
            });

            // For auto play video ads, it's recommended to load the ad
            // at least 30 seconds before it is shown
            interstitialAd.loadAd();
            showProgressDialog("Loading Video");
        }else {
            loadAdmobRewardedVideo();
        }
    }

    private void loadAdmobRewardedVideo(){
        mAd.loadAd("ca-app-pub-2649338194133294/8258689979", new AdRequest.Builder().build());
        showProgressDialog("Loading Video");
    }

    private void showProgressDialog(String message){
        RelativeLayout progressDialogLayout = (RelativeLayout) findViewById(R.id.progress_dialog);
        TextView progressDialogTV = (TextView) findViewById(R.id.progress_dialog_text);
        progressDialogTV.setText(message);
        progressDialogLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void onRewardedVideoAdLoaded() {
        hideProgressDialog();
        if (mAd.isLoaded()) {
            try {
                String categoryName = "Video";
                String label = "" + currentLevel.getLevelNo();
                String action = "Google video loaded";
                AnalyticsHelper.trackEvent(categoryName, action, label, this);
            } catch (Exception e) {
                e.printStackTrace();
            }
            mAd.show();
        }
    }

    @Override
    public void onRewardedVideoAdOpened() {

    }

    @Override
    public void onRewardedVideoStarted() {

    }

    @Override
    public void onRewardedVideoAdClosed() {
        try {
            String categoryName = "Video";
            String label = "" + currentLevel.getLevelNo();
            String action = "Google Video ad closed";
            AnalyticsHelper.trackEvent(categoryName, action, label, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRewarded(RewardItem rewardItem) {
        try {
            String categoryName = "Video";
            String label = "" + currentLevel.getLevelNo();
            String action = "Google Video ad completed";
            AnalyticsHelper.trackEvent(categoryName, action, label, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        alertDialogCustom.setResult(this, 60);
        alertDialogCustom.showWithAnimation();
    }

    @Override
    public void onRewardedVideoAdLeftApplication() {

    }

    @Override
    public void onRewardedVideoAdFailedToLoad(int i) {
        hideProgressDialog();
        if (adcolonyAd != null && !adcolonyAd.isExpired()){
            adcolonyAd.show();
        }else {
            if (listener != null && ad_options != null) {
                AdColony.requestInterstitial(ZONE_ID, listener, ad_options);
            }
            alertDialogCustom.setResultForSimpleDialog(this,"Error loading video", null);
            alertDialogCustom.showWithAnimation();
        }

    }

    @Override
    public void onRewardedVideoCompleted() {

    }

    private void hideProgressDialog(){
        RelativeLayout progressDialogLayout = (RelativeLayout) findViewById(R.id.progress_dialog);
        progressDialogLayout.setVisibility(View.GONE);
    }

    public void onVideoButtonClicked(View v){
        alertDialogCustom.setResultToWatchVideoForReward(this);
        alertDialogCustom.showWithAnimation();
        try {
            String categoryName = "Button";
            String label = "" + currentLevel.getLevelNo();
            String action = "Plus Clicked";
            AnalyticsHelper.trackEvent(categoryName, action, label, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onShuffleClick(View v){
        if (canvasView != null && currentLevel != null){
            canvasView.shuffleLetters();
        }
        try {
            String categoryName = "Button";
            String label = "" + currentLevel.getLevelNo();
            String action = "Shuffle Clicked";
            AnalyticsHelper.trackEvent(categoryName, action, label, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onHintClick(View v){
        //handle hint click
        if (currentLevel != null) {
            try {
                String categoryName = "Button";
                String label = "" + currentLevel.getLevelNo();
                String action = "Hint Clicked";
                AnalyticsHelper.trackEvent(categoryName, action, label, this);
            } catch (Exception e) {
                e.printStackTrace();
            }
            SharedPreferences pref = getSharedPreferences(NameConstant.GAME_DATABASE, MODE_PRIVATE);
            int hint = pref.getInt("guest_score", 0);
            if (hint >= 60) {
                WordConnectLevelLetter hintLetter = currentLevel.getLetterForNextHint();
                if (hintLetter != null) {
                    showLetter(hintLetter);
                    updateDiamondCount(-60);
                }
            } else {
                alertDialogCustom.setResultToWatchVideoForReward(this);
                alertDialogCustom.showWithAnimation();
            }
        }
    }

    private void showLetter(WordConnectLevelLetter letter){
        final View letterLayout = emptyPuzzleLayout.findViewWithTag("" + letter.getX() + "_" + letter.getY());
        if (letterLayout != null){
            letter.setLetterFilled(true);
            //ImageView button = letterLayout.findViewById(R.id.empty_letter_button);
            final View letterInnerLayout = letterLayout.findViewById(R.id.letter_layout);
            //button.setImageResource(Helper.getLetterImageResID((letter.getLetter())));
            //letterInnerLayout.setBackground(letter_background);
            SimpleDraweeView simpleDraweeView = letterLayout.findViewById(R.id.empty_letter_button);
            simpleDraweeView.setVisibility(View.VISIBLE);
            letterInnerLayout.setBackgroundColor(Color.TRANSPARENT);
            simpleDraweeView.setImageURI(letterBackgroundUri);
            TextView letterTextView = letterLayout.findViewById(R.id.letter_text);
            letterTextView.setText(String.valueOf(letter.getLetter()));
            letterTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP,(float) 0.24 * letterTextView.getWidth());
            letterTextView.setTag(-1);
            letterLayout.animate()
                    .scaleXBy(0.25f)
                    .scaleYBy(0.25f)
                    .setListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            letterLayout.animate()
                                    .scaleXBy(-0.25f)
                                    .scaleYBy(-0.25f)
                                    .setListener(null)
                                    .setDuration(200);
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    })
                    .setDuration(200);
        }
        if (currentLevel.isAllCharacterFilled()){
            onLevelCompleted();
        }
    }

    private void showToast(String message){
        toast.setVisibility(View.VISIBLE);
        toast.setText(message);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                toast.setVisibility(View.GONE);
            }
        },2000);
    }

    private void showTopToast(String message){
        topToast.setVisibility(View.VISIBLE);
        topToast.setText(message);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                topToast.setVisibility(View.GONE);
            }
        },2000);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus){
            hideNavigationBar();
        }
    }

    private void onLevelCompleted(){
        final Activity activity = this;
        if (!isDailyPuzzleWasAlreadyCompleted) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    alertDialogCustom.setResult(activity, currentLevel, isDailyPuzzle, gameData.getAdAfterStageCount());
                    if (isDailyPuzzle){
                        toast.setVisibility(View.VISIBLE);
                        toast.setText("You have completed today's puzzle.\nCome back tomorrow to play again.");
                    }
                    alertDialogCustom.showWithAnimation();
                }
            }, 1000);
            currentLevel.setCompleted(true, this, isDailyPuzzle);
        }
    }

    public void onShareLevelClick(View v){
        Helper.shareLevelImage(this, findViewById(R.id.main_content_layout));
        try {
            String categoryName = "Button";
            String label = "Level no " + currentLevel.getLevelNo();
            String action = "Level Share Clicked";
            AnalyticsHelper.trackEvent(categoryName, action, label, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onLevelListClick(View v){
        Intent intent = new Intent(this, WordConnectAllLevesActivity.class);
        startActivityForResult(intent,1);
        try {
            String categoryName = "Button";
            String label = "Level no " + currentLevel.getLevelNo();
            String action = "Level List Clicked";
            AnalyticsHelper.trackEvent(categoryName, action, label, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK ) {
            if (data != null
                    && data.getIntExtra("position", -1) != -1) {
                loadLevelIndex = data.getIntExtra("position", -1);
                loadNewStage();
            } else if (data != null && data.getBooleanExtra("isHomeClicked", false)){
                finish();
            }
        }
    }

    @Override
    protected void onDestroy() {
        adViewContainer.removeAllViews();
        super.onDestroy();
    }

    public void startSpinWheel(View view){
            if (!isWheelSpinnig && claimButtonSpinWheel.getVisibility() != View.VISIBLE){
                try {
                    String categoryName = "Spin";
                    String label = "" + currentLevel.getLevelNo();
                    String action = "Wheel Clicked";
                    AnalyticsHelper.trackEvent(categoryName, action, label, this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                RotateAnimation rotateInit = new RotateAnimation(
                        0, 0,
                        Animation.RELATIVE_TO_SELF, 0.5f,
                        Animation.RELATIVE_TO_SELF, 0.5f
                );
                rotateInit.setDuration(1);
                rotateInit.setFillAfter(false);
                rotateInit.setFillBefore(false);
                spinWheel.startAnimation(rotateInit);
                int max = 360;
                int min = 1;
                final int randomAngle = (int )(Math.random() * max + min);
                int rotationAngle = 360 * 15 + randomAngle;
                RotateAnimation rotate = new RotateAnimation(
                        0, rotationAngle,
                        Animation.RELATIVE_TO_SELF, 0.5f,
                        Animation.RELATIVE_TO_SELF, 0.5f
                );
                rotate.setDuration(4000);
                rotate.setFillAfter(true);
                rotate.setFillBefore(false);
                rotate.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        isWheelSpinnig = true;
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        isWheelSpinnig = false;
                        handleWheelSpinResult(getHintResult(randomAngle));
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                spinWheel.startAnimation(rotate);
            }
    }

    public void showSpinWheel(){
        RotateAnimation rotate = new RotateAnimation(
                0, 0,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f
        );
        rotate.setDuration(10);
        rotate.setFillAfter(false);
        rotate.setFillBefore(false);
        spinWheel.startAnimation(rotate);
        spinLayout.setVisibility(View.VISIBLE);
        spinTextView.setText("Touch Wheel to spin");
        showCloseButtonSpinWheel();
        spinTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spinLayout.setVisibility(View.GONE);
            }
        });
        try {
            String categoryName = "Spin";
            String label = "" + currentLevel.getLevelNo();
            String action = "Spin Showed";
            AnalyticsHelper.trackEvent(categoryName, action, label, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int getHintResult(int angle){
        if (1 <= angle  && angle <= 45){
            return ONE_MORE_SPIN;
        } else if (46 <= angle  && angle <= 90){
            return FIFTY_DIAMONDS;
        } else if (91 <= angle  && angle <= 135){
            return TWO_HINTS;
        } else if (136 <= angle  && angle <= 180){
            return FORTY_DIAMONDS;
        } else if (181 <= angle  && angle <= 225){
            return WORD_HINT;
        } else if (226 <= angle  && angle <= 270){
            return THIRTY_DIAMONDS;
        } else if (271 <= angle  && angle <= 315){
            return ONE_HINT;
        } else if (316 <= angle  && angle <= 360){
            return TWENTY_DIAMONDS;
        } else {
            return ONE_MORE_SPIN;
        }

    }

    private void handleWheelSpinResult(int result){
        switch (result){
            case ONE_MORE_SPIN:
                spinResult = ONE_MORE_SPIN;
                spinTextView.setText("You Won one more spin.\nTouch to Spin Again.");
                showCloseButtonSpinWheel();
                break;
            case FIFTY_DIAMONDS:
                spinResult = FIFTY_DIAMONDS;
                spinTextView.setText("You Won Fifty Diamonds");
                showClaimButtonSpinWheel();
                break;
            case TWO_HINTS:
                spinResult = TWO_HINTS;
                spinTextView.setText("You Won Two Hints");
                showClaimButtonSpinWheel();
                break;
            case FORTY_DIAMONDS:
                spinResult = FORTY_DIAMONDS;
                spinTextView.setText("You Won Forty Diamonds");
                showClaimButtonSpinWheel();
                break;
            case WORD_HINT:
                spinResult = WORD_HINT;
                spinTextView.setText("You Won Word Hint");
                showClaimButtonSpinWheel();
                break;
            case THIRTY_DIAMONDS:
                spinResult = THIRTY_DIAMONDS;
                spinTextView.setText("You Won Thirty Diamonds");
                showClaimButtonSpinWheel();
                break;
            case ONE_HINT:
                spinResult = ONE_HINT;
                spinTextView.setText("You Won One Hint");
                showClaimButtonSpinWheel();
                break;
            case TWENTY_DIAMONDS:
                spinResult = TWENTY_DIAMONDS;
                spinTextView.setText("You Won Twenty Diamonds");
                showClaimButtonSpinWheel();
                break;
            default:
                spinResult = ONE_MORE_SPIN;
                spinTextView.setText("You Won one more spin.\nTouch to Spin Again.");
                showCloseButtonSpinWheel();
                break;
        }
    }

    private void claimWheelSpinResult(int result){
        WordConnectLevelLetter hintLetter;
        switch (result){
            case FIFTY_DIAMONDS:
                updateDiamondCount(50);
                break;
            case TWO_HINTS:
                hintLetter = currentLevel.getLetterForNextHint();
                if (hintLetter != null) {
                    showLetter(hintLetter);
                }
                hintLetter = currentLevel.getLetterForNextHint();
                if (hintLetter != null) {
                    showLetter(hintLetter);
                }
                break;
            case FORTY_DIAMONDS:
                updateDiamondCount(40);
                break;
            case WORD_HINT:
                claimWordHint();
                break;
            case THIRTY_DIAMONDS:
                updateDiamondCount(30);
                break;
            case ONE_HINT:
                hintLetter = currentLevel.getLetterForNextHint();
                if (hintLetter != null) {
                    showLetter(hintLetter);
                }
                break;
            case TWENTY_DIAMONDS:
                updateDiamondCount(20);
                break;
            default:
                spinTextView.setText("You Won one more spin.\nTouch to Spin Again.");
                showCloseButtonSpinWheel();
                break;
        }
    }

    public void onSpinClaimClick(View v){
        spinLayout.setVisibility(View.GONE);
        claimWheelSpinResult(spinResult);
        hideSpinWheelIcon();
        try {
            String categoryName = "Spin";
            String label = "" + currentLevel.getLevelNo();
            String action = "Spin Claimed";
            AnalyticsHelper.trackEvent(categoryName, action, label, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onSpinCloseClicked(View v){
        spinLayout.setVisibility(View.GONE);
        try {
            String categoryName = "Spin";
            String label = "" + currentLevel.getLevelNo();
            String action = "Close Clicked";
            AnalyticsHelper.trackEvent(categoryName, action, label, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showCloseButtonSpinWheel(){
        closeButtonSpinWheel.setVisibility(View.VISIBLE);
        claimButtonSpinWheel.setVisibility(View.GONE);
    }

    private void showClaimButtonSpinWheel(){
        closeButtonSpinWheel.setVisibility(View.GONE);
        claimButtonSpinWheel.setVisibility(View.VISIBLE);
    }

    private void claimWordHint(){
        String word = currentLevel.getNextUnformedWord();
        if (!TextUtils.isEmpty(word)){
            showWordFormed(word);
        }
    }

    private void showWordFormed(String word){
        List<WordConnectLevelLetter> letterOfWordFormed = currentLevel.getLettersOfWordsFormed(word);
        if (letterOfWordFormed.size() == word.length()){
            //animateDiamond(currentLevel.getWordPoint());
            Helper.playGameSound(this, R.raw.word_creation_sound);
            currentLevel.addWordsFormed(this,word, isDailyPuzzle);
            for (WordConnectLevelLetter letter: letterOfWordFormed){
                showLetter(letter);
            }
        }
    }

    public void onSpinIconClick(View view) {
        try {
            String categoryName = "Button";
            String label = "" + currentLevel.getLevelNo();
            String action = "Spin Clicked";
            AnalyticsHelper.trackEvent(categoryName, action, label, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        showSpinWheel();
    }

    private void showSpinWheelIcon(){
        if (isSpinWheelEnabled()) {
            spinIconButton.setVisibility(View.VISIBLE);
        }
    }

    private void hideSpinWheelIcon(){
        spinIconButton.setVisibility(View.GONE);
        setIsSpinWheelEnabled(false);
    }

    private boolean isSpinWheelEnabled(){
        SharedPreferences pref = getSharedPreferences(NameConstant.GAME_DATABASE, MODE_PRIVATE);
        boolean isSpinWheelEnabled = pref.getBoolean(NameConstant.IS_SPIN_WHEEL_ENABLED, false);
        if (!isSpinWheelEnabled){
            try {
                Long lastDisabledTime = pref.getLong(NameConstant.LAST_IS_SPIN_WHEEL_DISABLED, 0);
                if (lastDisabledTime == 0){
                    setIsSpinWheelEnabled(true);
                    isSpinWheelEnabled = true;
                }else {
                    long MAX_DURATION = MILLISECONDS.convert(gameData.getSpinAfterMin(), MINUTES);
                    long duration = Calendar.getInstance().getTimeInMillis() - lastDisabledTime;
                    if (duration >= MAX_DURATION) {
                        setIsSpinWheelEnabled(true);
                        isSpinWheelEnabled = true;
                    }
                }
            }catch (Exception ex){
                isSpinWheelEnabled = true;
            }
        }
        return isSpinWheelEnabled;
    }

    private void setIsSpinWheelEnabled(boolean isSpinWheelEnabled){
        SharedPreferences.Editor editor = getSharedPreferences(NameConstant.GAME_DATABASE, MODE_PRIVATE).edit();
        editor.putBoolean(NameConstant.IS_SPIN_WHEEL_ENABLED, isSpinWheelEnabled);
        if (!isSpinWheelEnabled){
            editor.putLong(NameConstant.LAST_IS_SPIN_WHEEL_DISABLED, Calendar.getInstance().getTimeInMillis());
        }
        editor.apply();
    }
}
