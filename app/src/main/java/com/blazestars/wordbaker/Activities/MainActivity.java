package com.blazestars.wordbaker.Activities;

import android.animation.Animator;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.blazestars.wordbaker.Adapters.StageAdapter;
import com.blazestars.wordbaker.BlazeStarsApplication;
import com.blazestars.wordbaker.CustomViews.AlertDialogCustom;
import com.blazestars.wordbaker.Helper.APICallHelper;
import com.blazestars.wordbaker.Helper.AnalyticsHelper;
import com.blazestars.wordbaker.Helper.Helper;
import com.blazestars.wordbaker.Helper.MyInterstitialAd;
import com.blazestars.wordbaker.Interfaces.GameScreenListner;
import com.blazestars.wordbaker.Modal.GameData;
import com.blazestars.wordbaker.Modal.NameConstant;
import com.blazestars.wordbaker.Network.VolleyData;
import com.blazestars.wordbaker.R;
import com.facebook.common.util.UriUtil;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity implements GameScreenListner{

    GameData gameData;
    Activity activity;
    AlertDialogCustom alertDialogCustom;
    ProgressBar progressBar;
    View playButton, exitButton;
    View actualDiamond;
    TextView diamondCountTv;
    public static AdView adView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        activity = this;
        if (adView == null) {
            adView = new AdView(this);
            adView.setAdSize(AdSize.SMART_BANNER);
            adView.setAdUnitId("ca-app-pub-2649338194133294/1570880747");
            AdRequest adRequest = new AdRequest.Builder().build();
            adView.loadAd(adRequest);
        }
        initViewsAndData();
    }

    private void initViewsAndData(){
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        playButton = findViewById(R.id.play_button);
        exitButton = findViewById(R.id.exit_button);
        loadGameData();
        //Bitmap image;// = Helper.getBitmap(this, R.drawable.game_background_25, Bitmap.CompressFormat.JPEG);
        //letterBackground = Helper.getDrawable(this, R.drawable.letter_background, Bitmap.CompressFormat.PNG);
        //((ImageView)findViewById(R.id.background_image)).setImageBitmap(image);

// uri looks like res:/123456789
        ((SimpleDraweeView)findViewById(R.id.my_image_view)).setImageURI(Helper.
                getURIForResourceImage(R.drawable.game_background_25));
        alertDialogCustom = (AlertDialogCustom) findViewById(R.id.alert_dialog_reward);
        if (getIntent().getBooleanExtra("isHint", false)
                || "OPEN_APP_WITH_HINT".equalsIgnoreCase(getIntent().getStringExtra("notification_type"))){
            try {
                alertDialogCustom.setResult(this, Integer.valueOf(getIntent().getStringExtra("diamond_count")));
                alertDialogCustom.showWithAnimation();
            }catch (Exception ex){
                alertDialogCustom.setResult(this, 10);
                alertDialogCustom.showWithAnimation();
            }
            try {
                String pageviews = "Daily Reward from notification";
                AnalyticsHelper.trackPageView(pageviews, this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            String pageviews = "Home";
            AnalyticsHelper.trackPageView(pageviews, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            final TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            final String simCountry = tm.getSimCountryIso();
            if (TextUtils.isEmpty(simCountry)) {
                FirebaseMessaging.getInstance().subscribeToTopic("notification_word_baker");
                FirebaseMessaging.getInstance().subscribeToTopic("daily_puzzle_notification_word_baker");
            }else {
                Log.v("country : ", simCountry);
                FirebaseMessaging.getInstance().subscribeToTopic("notification_word_baker_" + simCountry);
                FirebaseMessaging.getInstance().subscribeToTopic("daily_puzzle_notification_word_baker_" + simCountry);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        SharedPreferences pref = getSharedPreferences(NameConstant.GAME_DATABASE, MODE_PRIVATE);
        boolean isFirstLaunch = pref.getBoolean("is_first_launch",true);
        SharedPreferences.Editor editor = getSharedPreferences(NameConstant.GAME_DATABASE, MODE_PRIVATE).edit();
        if (isFirstLaunch){

            editor.putInt("hint", 3);
            editor.putBoolean("is_first_launch",false);
            editor.apply();
            openHowToPlayActivity();
        }
        String lastLaunchDate = pref.getString("last_launch_date","");
        String formattedDate = Helper.getCurrentDate();
        if (!lastLaunchDate.equalsIgnoreCase(formattedDate)){
            alertDialogCustom.setResult(this, 10, true);
            alertDialogCustom.showWithAnimation();
            try {
                String pageviews = "Daily Reward with out notification";
                AnalyticsHelper.trackPageView(pageviews, this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        editor = getSharedPreferences(NameConstant.GAME_DATABASE, MODE_PRIVATE).edit();
        editor.putString("last_launch_date", formattedDate);
        editor.apply();
        diamondCountTv = (TextView) findViewById(R.id.diamond_count_tv);
        actualDiamond = findViewById(R.id.actual_diamond);
        int diamondCount = pref.getInt("guest_score",0);
        diamondCountTv.setText(String.valueOf(diamondCount));
        if (getIntent().getBooleanExtra("isForDailyPuzzle", false)
                || "OPEN_APP_FOR_DAILY_PUZZLE".equalsIgnoreCase(getIntent().getStringExtra("notification_type"))){
            try {
                String pageviews = "Daily puzzle notification";
                AnalyticsHelper.trackPageView(pageviews, this);
            } catch (Exception e) {
                e.printStackTrace();
            }
            onDailyPuzzleClick(new View(this));
        }



        ((SimpleDraweeView)actualDiamond).setImageURI(Helper.
                getURIForResourceImage(R.drawable.diamond));

        ((SimpleDraweeView)exitButton).setImageURI(Helper.
                getURIForResourceImage(R.drawable.daily_puzzle_button));

        ((SimpleDraweeView)playButton).setImageURI(Helper.
                getURIForResourceImage(R.drawable.play_button));

        ((SimpleDraweeView)findViewById(R.id.share_button)).setImageURI(Helper.
                getURIForResourceImage(R.drawable.share_button));

        ((SimpleDraweeView)findViewById(R.id.rate_button)).setImageURI(Helper.
                getURIForResourceImage(R.drawable.rate_it_button));

        /*image = new BitmapDrawable(getResources(),
                BitmapFactory.decodeResource(getResources(),R.drawable.round_rectangle_white));
        ((ImageView)findViewById(R.id.round_rectangle)).setImageDrawable(image);

        image = new BitmapDrawable(getResources(),
                BitmapFactory.decodeResource(getResources(),R.drawable.letter_background));
                */
        Uri uri = Helper.getURIForResourceImage(R.drawable.letter_background);
        ((SimpleDraweeView)findViewById(R.id.letter_1)).setImageURI(uri);
        ((SimpleDraweeView)findViewById(R.id.letter_2)).setImageURI(uri);
        ((SimpleDraweeView)findViewById(R.id.letter_3)).setImageURI(uri);
        ((SimpleDraweeView)findViewById(R.id.letter_4)).setImageURI(uri);
        ((SimpleDraweeView)findViewById(R.id.letter_5)).setImageURI(uri);
        ((SimpleDraweeView)findViewById(R.id.letter_6)).setImageURI(uri);
        ((SimpleDraweeView)findViewById(R.id.letter_7)).setImageURI(uri);
        ((SimpleDraweeView)findViewById(R.id.letter_8)).setImageURI(uri);
        ((SimpleDraweeView)findViewById(R.id.letter_9)).setImageURI(uri);
    }

    public void onPlayClick(View v){
        if (gameData != null) {
            //Intent intent = new Intent(this, GameActivity.class);
            Intent intent = new Intent(this, WordConnectGameCanvasActivity.class);
            startActivity(intent);
        }else {
            Toast.makeText(this, "Loading data, Please wait..", Toast.LENGTH_LONG).show();
            loadGameData();
        }
        try {
            String categoryName = "Button";
            String label = "Home screen";
            String action = "Play Clicked";
            AnalyticsHelper.trackEvent(categoryName, action, label, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onShareClick(View v){
        try{
            String title = "Play Word Baker puzzle game, to download click the below link :";
            String appLink = "\n http://play.google.com/store/apps/details?id=" + getPackageName()+"\n";
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, title + appLink);
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
            try {
                String categoryName = "Button";
                String label = "Home screen";
                String action = "Share Clicked";
                AnalyticsHelper.trackEvent(categoryName, action, label, this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }catch (Exception ex){

        }

    }

    public void onDailyPuzzleClick(View v) {
        try {
            if (v != null) {
                String categoryName = "Button";
                String label = "Home screen";
                String action = "Daily puzzle Clicked";
                AnalyticsHelper.trackEvent(categoryName, action, label, this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (gameData != null) {
            Intent intent = new Intent(this, WordConnectGameCanvasActivity.class);
            intent.putExtra(NameConstant.INTENT_DATA_KEY_IS_DAILY_PUZZLE, true);
            startActivity(intent);
        } else {
            Toast.makeText(this, "Loading data, Please wait..", Toast.LENGTH_LONG).show();
            loadGameData();
        }
    }

    public void onRateItClicked(View v){
        try {
            String categoryName = "Button";
            String label = "Home screen";
            String action = "Rate It Clicked";
            AnalyticsHelper.trackEvent(categoryName, action, label, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Uri uri = Uri.parse("market://details?id=" + getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + getPackageName())));
        }
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    private void loadGameData(){
            String url = NameConstant.GET_GAME_DATA_URL;// + "?date=" + Helper.getCurrentDate();
            new VolleyData(this){

                @Override
                protected void VPreExecute() {
                    if (progressBar != null)
                        progressBar.setVisibility(View.VISIBLE);
                }

                @Override
                protected void VResponse(JSONObject response, String tag) {
                    gameData = new Gson().fromJson(response.toString(),GameData.class);
                    if (gameData != null){
                        if (gameData.getStages() != null && gameData.getStages().size() > 0){
                            gameData.getStages().get(0).openStage(activity);
                        }
                        gameData.setLockUnlockedStateFromSharedPref(activity);
                    }
                    if (progressBar != null)
                        progressBar.setVisibility(View.GONE);
                    if (playButton != null)
                        playButton.setVisibility(View.VISIBLE);
                    if (exitButton != null)
                        exitButton.setVisibility(View.VISIBLE);
                }

                @Override
                protected void VError(VolleyError error, String tag) {
                    if (progressBar != null)
                        progressBar.setVisibility(View.GONE);
                    if (playButton != null)
                        playButton.setVisibility(View.VISIBLE);
                    if (exitButton != null)
                        exitButton.setVisibility(View.VISIBLE);
                }
            }.getJsonObject(url,"Game Data",this,true);

            //will refersh cache
            new VolleyData(this){
                @Override
                protected void VPreExecute() {
                }

                @Override
                protected void VResponse(JSONObject response, String tag) {

                }

                @Override
                protected void VError(VolleyError error, String tag) {

                }
            }.getJsonObject(url,"Game Data Cache",this,false);
    }


    private void hideNavigationBar(){
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    @Override
    protected void onResume() {
        super.onResume();
        hideNavigationBar();
    }

    @Override
    public void loadNewStage() {

    }

    @Override
    public void loadVideo() {

    }

    @Override
    public void animateDiamond(final int diamondEarned) {
        actualDiamond.animate()
                .scaleXBy(1)
                .scaleYBy(1)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        actualDiamond.animate()
                                .scaleXBy(-1)
                                .scaleYBy(-1)
                                .setListener(null)
                                .setDuration(300);
                        updateDiamondCount(diamondEarned);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .setDuration(300);
    }

    @Override
    public void wordFormedInCanvas(String word) {
        Toast.makeText(this,word,Toast.LENGTH_LONG).show();
    }

    @Override
    public void onLetterSelectedInCanvas(String word) {

    }

    private void openHowToPlayActivity(){
        //Intent intent = new Intent(this,HowToPlayActivity.class);
        //startActivity(intent);
    }

    private void updateDiamondCount(int diamondEarned){
        SharedPreferences preferences = getSharedPreferences(NameConstant.GAME_DATABASE, MODE_PRIVATE);
        SharedPreferences.Editor editor = getSharedPreferences(NameConstant.GAME_DATABASE, MODE_PRIVATE).edit();
        int score = preferences.getInt("guest_score",0);
        score = score + diamondEarned;
        editor.putInt("guest_score",score);
        editor.apply();
        Helper.playGameSound(this, R.raw.diamond_earned_sound);
        diamondCountTv.setText(String.valueOf(score));

    }

    @Override
    protected void onDestroy() {
        if (adView != null) {
            adView.destroy();
            adView = null;
        }
        super.onDestroy();
    }
}
