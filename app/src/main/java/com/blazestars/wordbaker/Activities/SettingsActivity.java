package com.blazestars.wordbaker.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.blazestars.wordbaker.Helper.AnalyticsHelper;
import com.blazestars.wordbaker.Helper.Helper;
import com.blazestars.wordbaker.R;

import java.util.ArrayList;

public class SettingsActivity extends AppCompatActivity {

    ListView lv;
    ArrayList<String> listItems;
    Context context;
    Switch.OnCheckedChangeListener onOffChangeListner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((TextView) findViewById(R.id.title)).setText("Settings");
        context = this;
        lv = (ListView) findViewById(R.id.settingList);
        setListItems();
        lv.setAdapter(new settingsListAdapter(this,R.layout.settings_list_item,listItems));
        setSettingListOnClickListner();
        try {
            String pageviews = "Settings";
            AnalyticsHelper.trackPageView(pageviews, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void backButtonTapped(View v)
    {
        super.onBackPressed();
    }

    private void setListItems(){
        listItems = new ArrayList<String>();
        listItems.add("Sound");
        listItems.add("About Us");
    }

    private void setSettingListOnClickListner(){

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home)
        {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class settingsListAdapter extends ArrayAdapter<String> {
        Context context;
        ArrayList<String> data;
        public settingsListAdapter(Context context, int layoutResourceId, ArrayList<String> data){
            super(context,layoutResourceId,data);
            this.context = context;
            this.data =data;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (data.get(position).equalsIgnoreCase("About Us")){
                convertView = inflater.inflate(R.layout.settings_list_last_item, parent, false);
                TextView privacyPolicy = (TextView)convertView.findViewById(R.id.privacy_policy);
                TextView termsAndConditions = (TextView)convertView.findViewById(R.id.terms_and_conditions);
                privacyPolicy.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context,AboutUs.class);
                        intent.putExtra("header","privacypolicy");
                        startActivity(intent);
                    }
                });
                termsAndConditions.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context,AboutUs.class);
                        intent.putExtra("header","termsandconditions");
                        startActivity(intent);
                    }
                });
                return convertView;
            }if (data.get(position).equalsIgnoreCase("sound"))
            {
                TextView tv_item;
                convertView = inflater.inflate(R.layout.settings_push_notification_item, parent, false);
                tv_item = (TextView) convertView.findViewById(R.id.item);
                tv_item.setText("Sound");
                final Switch onOff = (Switch) convertView.findViewById(R.id.onOff);
                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onOff.toggle();
                    }
                });
                onOffChangeListner = new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (!isChecked) {
                            if (Helper.isSoundEnabled(context)) {
                                Helper.setSoundOnOff(context, false);
                                Toast.makeText(context, " Sound Disabled ", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            if (!Helper.isSoundEnabled(context)) {
                                Helper.setSoundOnOff(context, true);
                                Toast.makeText(context, " Sound Enabled ", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                };
                onOff.setOnCheckedChangeListener(onOffChangeListner);
                if (Helper.isSoundEnabled(context)) {
                    onOff.setChecked(true);
                }
                else {
                    onOff.setChecked(false);
                }
                return convertView;

            }
            return null;
        }

    }
}
