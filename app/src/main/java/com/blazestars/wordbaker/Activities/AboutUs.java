package com.blazestars.wordbaker.Activities;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

import com.blazestars.wordbaker.Helper.AnalyticsHelper;
import com.blazestars.wordbaker.R;


public class AboutUs extends AppCompatActivity {
    TextView mTitleTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_about_us);
        mTitleTV = (TextView) findViewById(R.id.title);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        String header = getIntent().getExtras().getString("header","");

            if (header.equalsIgnoreCase("aboutus")) {
                mTitleTV.setText("About Us");
                loadAboutUsData();
            } else if (header.equalsIgnoreCase("privacypolicy")) {
                mTitleTV.setText("Privacy Policy");
                loadPrivacyPolicyData();
            } else if (header.equalsIgnoreCase("termsandconditions")) {
                mTitleTV.setText("Terms And Conditions");
                loadTermsAndConditionsData();
            } else {
                onBackPressed();
            }
        try {
            String pageviews = "About_US";
            AnalyticsHelper.trackPageView(pageviews, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
       // getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home)
        {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void backButtonTapped(View v)
    {
        super.onBackPressed();
    }

    private void loadAboutUsData(){
        String webstr = "<html><head><style>@font-face {font-family:'opensans';src: url('file:///android_asset/fonts/regular.ttf';font-weight: normal);}body {font-family:'opensans';}img{PADDING-BOTTOM: 5px;max-width:100%;}</style><script>window.onload = callOnload;window.onscroll=callOnScroll;function showLoader(){document.getElementById('loader').style.display='inline'};function hideLoader(){document.getElementById('loader').style.display='none'};function toggleFont(){var d=document.getElementById('multicolumn');var d1=document.getElementById('headline');if(d.className == 'large'){d.className='';}else{d.className='large';}if(d1.className == 'large'){d1.className='title';}else{d1.className='large';}};function callOnload(){ hideLoader();}</script></head><body >";
        webstr += "<p style='font-family:\"opensans\";line-height:25px;text-align:justify;color:'#00000'; font-size:18;'>";
        webstr += getResources().getString(R.string.our_mission)+"</p>";
        webstr += "</body></html>";
        WebView webView = (WebView)findViewById(R.id.webViewAboutUs);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadDataWithBaseURL("", webstr, "text/html", "UTF-8", "");
        webView.setBackgroundColor(Color.WHITE);
    }
    private void loadPrivacyPolicyData(){

            String webstr = "<html><head><style>@font-face {font-family:'opensans';src: url('file:///android_asset/fonts/regular.ttf';font-weight: normal);}body {font-family:'opensans';}img{PADDING-BOTTOM: 5px;max-width:100%;}</style><script>window.onload = callOnload;window.onscroll=callOnScroll;function showLoader(){document.getElementById('loader').style.display='inline'};function hideLoader(){document.getElementById('loader').style.display='none'};function toggleFont(){var d=document.getElementById('multicolumn');var d1=document.getElementById('headline');if(d.className == 'large'){d.className='';}else{d.className='large';}if(d1.className == 'large'){d1.className='title';}else{d1.className='large';}};function callOnload(){ hideLoader();}</script></head><body >";
            webstr += "<p style='font-family:\"opensans\";line-height:25px;text-align:justify;color:'#00000'; font-size:18;'>";
            webstr += getResources().getString(R.string.privacy_policy) + "</p>";
            webstr += "</body></html>";
            WebView webView = (WebView) findViewById(R.id.webViewAboutUs);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.loadDataWithBaseURL("", webstr, "text/html", "UTF-8", "");
            webView.setBackgroundColor(Color.WHITE);

    }
    private void loadTermsAndConditionsData(){

            String webstr = "<html><head><style>@font-face {font-family:'opensans';src: url('file:///android_asset/fonts/regular.ttf';font-weight: normal);}body {font-family:'opensans';}img{PADDING-BOTTOM: 5px;max-width:100%;}</style><script>window.onload = callOnload;window.onscroll=callOnScroll;function showLoader(){document.getElementById('loader').style.display='inline'};function hideLoader(){document.getElementById('loader').style.display='none'};function toggleFont(){var d=document.getElementById('multicolumn');var d1=document.getElementById('headline');if(d.className == 'large'){d.className='';}else{d.className='large';}if(d1.className == 'large'){d1.className='title';}else{d1.className='large';}};function callOnload(){ hideLoader();}</script></head><body >";
            webstr += "<p style='font-family:\"opensans\";line-height:25px;text-align:justify;color:'#00000'; font-size:18;'>";
            webstr += getResources().getString(R.string.terms_and_conditions) + "</p>";
            webstr += "</body></html>";
            WebView webView = (WebView) findViewById(R.id.webViewAboutUs);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.loadDataWithBaseURL("", webstr, "text/html", "UTF-8", "");
            webView.setBackgroundColor(Color.WHITE);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
