package com.blazestars.wordbaker.Activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.blazestars.wordbaker.Helper.AnalyticsHelper;
import com.blazestars.wordbaker.Helper.MyInterstitialAd;
import com.blazestars.wordbaker.Modal.NameConstant;
import com.blazestars.wordbaker.Modal.Stage;
import com.blazestars.wordbaker.R;
import com.google.android.gms.ads.InterstitialAd;
import com.squareup.picasso.Picasso;

public class LevelScoreActivity extends AppCompatActivity {

    Context mContext;
    int score = 0;
    Stage stage;
    int levelPosition;
    MyInterstitialAd interstitialAd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level_score);
        mContext = this;
        SharedPreferences.Editor editor = getSharedPreferences(NameConstant.GAME_DATABASE, MODE_PRIVATE).edit();
        SharedPreferences preferences = getSharedPreferences(NameConstant.GAME_DATABASE,MODE_PRIVATE);
        score = preferences.getInt("guest_score",0);
        stage = getIntent().getParcelableExtra("stage");
        levelPosition = getIntent().getExtras().getInt("position",0);
        if (!getIntent().getBooleanExtra("isLevelAlreadyCompleted",false)) {
            score = score + stage.getLevels().get(levelPosition).getPoints();
        }
        editor.putInt("guest_score",score);
        editor.apply();
        TextView scoreTV = (TextView) findViewById(R.id.score);
        scoreTV.setText("Total Score : " + score);
        TextView stageName = (TextView) findViewById(R.id.stage_name);
        stageName.setText("Your " + stage.getName() + " is");
        TextView percentageTV = (TextView) findViewById(R.id.percentage);
        int correctLevelPosition = levelPosition + 1;
        int percentage = (int) (((float) correctLevelPosition / (float) stage.getLevels().size()) * (float) 100);
        percentageTV.setText((String.valueOf(percentage)) + " %");
        ImageView imageView = (ImageView) findViewById(R.id.stage_image);
        if (percentage == 100){
            percentageTV.setVisibility(View.GONE);
            Picasso.with(this).load(stage.getImage()).into(imageView);
            imageView.setVisibility(View.VISIBLE);
            interstitialAd = new MyInterstitialAd(this,false);
            findViewById(R.id.playNextStageButton).setVisibility(View.VISIBLE);
        }else {
            android.os.Handler handler = new android.os.Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    setResult(RESULT_OK);
                    finish();
                }
            }, 2000);
        }
        try {
            String pageviews = "Score";
            AnalyticsHelper.trackPageView(pageviews, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    public void playNextStageClicked(View v){
        if (interstitialAd != null){
            interstitialAd.showAd();
            setResult(RESULT_OK);
            finish();
        }else {
            setResult(RESULT_OK);
            finish();
        }
    }
}
