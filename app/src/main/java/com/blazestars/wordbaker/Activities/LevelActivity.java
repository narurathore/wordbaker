package com.blazestars.wordbaker.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.blazestars.wordbaker.Adapters.LevelAdapter;
import com.blazestars.wordbaker.Helper.AnalyticsHelper;
import com.blazestars.wordbaker.Modal.Stage;
import com.blazestars.wordbaker.R;

public class LevelActivity extends AppCompatActivity {

    int REQUEST_CODE_GAME_LEVELSCREEN = 1;
    RecyclerView recyclerView;
    LevelAdapter levelAdapter;
    Stage stage;
    Stage nextStage;
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level);
        mContext = this;
        getIntentData();
        initiateUIComponents();
        try {
            String pageviews = "Stage_opened_" + stage.getName();
            AnalyticsHelper.trackPageView(pageviews, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getIntentData() {
        stage = getIntent().getParcelableExtra("stage");
        if (getIntent().hasExtra("nextStage")) {
            nextStage = getIntent().getParcelableExtra("nextStage");
        }
        boolean openGame = getIntent().getBooleanExtra("openGame",false);
        if (openGame){
            int levelNumber = getIntent().getIntExtra("levelNumber",0);
            if (levelNumber < stage.getLevels().size()) {
                openGameScreen(levelNumber);
            }
        }
    }

    private void initiateUIComponents(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView titleTV = (TextView) toolbar.findViewById(R.id.title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        titleTV.setText(stage.getName());
        recyclerView = (RecyclerView) findViewById(R.id.level_recycler_view);
        levelAdapter = new LevelAdapter(this, stage.getLevels());
        levelAdapter.setOnItemClickListener(new LevelAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                openGameScreen(position);
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(levelAdapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_GAME_LEVELSCREEN) {
            if (resultCode == RESULT_OK) {
                setResult(RESULT_OK);
                finish();
            }else {
                stage.updateLockedStateFromSharedPref(this);
                stage.updateCompletedStateFromSharedPref(this);
                levelAdapter.notifyDataSetChanged();
            }
        }
    }

    private void openGameScreen(int position){
        Intent intent = new Intent(mContext,GameActivity.class);
        intent.putExtra("position",position);
        intent.putExtra("stage",stage);
        if (nextStage != null){
            intent.putExtra("nextStage",nextStage);
        }
        startActivityForResult(intent,REQUEST_CODE_GAME_LEVELSCREEN);
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home)
        {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }
}
