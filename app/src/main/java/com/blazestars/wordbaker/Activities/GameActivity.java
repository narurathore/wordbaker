package com.blazestars.wordbaker.Activities;

import android.animation.Animator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.adcolony.sdk.AdColony;
import com.adcolony.sdk.AdColonyAdOptions;
import com.adcolony.sdk.AdColonyAppOptions;
import com.adcolony.sdk.AdColonyInterstitial;
import com.adcolony.sdk.AdColonyInterstitialListener;
import com.adcolony.sdk.AdColonyReward;
import com.adcolony.sdk.AdColonyRewardListener;
import com.adcolony.sdk.AdColonyZone;
import com.blazestars.wordbaker.CustomViews.AlertDialogCustom;
import com.blazestars.wordbaker.Helper.AnalyticsHelper;
import com.blazestars.wordbaker.Helper.Helper;
import com.blazestars.wordbaker.Interfaces.GameScreenListner;
import com.blazestars.wordbaker.Modal.GameData;
import com.blazestars.wordbaker.Modal.Level;
import com.blazestars.wordbaker.Modal.NameConstant;
import com.blazestars.wordbaker.Modal.Stage;
import com.blazestars.wordbaker.R;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class GameActivity extends AppCompatActivity implements RewardedVideoAdListener, GameScreenListner {

    final int REQUEST_CODE_SCORE_CARD = 1;
    final private String APP_ID = "app987457e05af34ec194";
    final private String ZONE_ID = "vza620d2c35cc748a897";
    final private String TAG = "AdColonyDemo";

    public AdColonyInterstitial adcolonyAd;
    private AdColonyInterstitialListener listener;
    private AdColonyAdOptions ad_options;


    RelativeLayout mPlayScreenLayout;
    LinearLayout emptyWordsContainer;
    Context mContext;
    Activity mActivity;
    ArrayList<View> letterButtons = new ArrayList<>();
    ArrayList<View> lettersMarked = new ArrayList<>();
    ArrayList<String> words = new ArrayList<String>();
    TextView lastTriedWord;
    int numRows = 3;
    int numCols = 3;
    int LETTER_WIDTH = 400;
    int letterLeftMargin = 0;
    final int MARGIN_BETWEEN_LETTERS = 48;
    final int LETTER_BUTTON_MARGIN = 20;
    int SCREEN_WIDTH;
    Map<String,String> lettersInitialCoordinates = new HashMap<>();
    Level currentLevel;
    Stage stage;
    Stage nextStage;
    int currentPosition = 0;
    int touchMargin;
    View ivChefCap;
    int totalHintUsed = 0;
    int[] wordHintUsed;
    static int levelPlayedCount;
    private RewardedVideoAd mAd;
    private AlertDialogCustom alertDialogCustom;
    private View actualDiamond;
    private TextView diamondCountTv;
    private MediaPlayer mpMusic;
    GameData gameData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        mActivity = this;
        mContext = this;
        getIntentData();
        try {
            String pageviews = "Game_Screen_Opened_" + stage.getName() + "_" + currentLevel.getName();
            AnalyticsHelper.trackPageView(pageviews, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        initializeViews();
        loadLevel();
        AdView adView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        mAd = MobileAds.getRewardedVideoAdInstance(this);
        mAd.setRewardedVideoAdListener(this);
        initAdcolony();

    }

    private void loadLevel(){
        stage = gameData.getLastStageUnlocked();
        if (stage == null){
            //Game Completed
            Toast.makeText(this, "Game Completed", Toast.LENGTH_LONG).show();
            return;
        }
        currentLevel = stage.getLastLevelUnlocked();
        levelPlayedCount++;
        lastTriedWord.setText("");
        currentLevel.setLocked(false,this);
        TextView titleTV = (TextView) findViewById(R.id.level_name);
        int levelNo = (stage.getStageNo() - 1) * stage.getLevels().size() + currentLevel.getLevelNo();
        titleTV.setText("Level: " + levelNo);
        initializeData();
        addLettersOfPuzzle();
        try {
            String categoryName = "Game";
            String label = stage.getName() + "_" + currentLevel.getName();
            String action = "Played";
            AnalyticsHelper.trackEvent(categoryName, action, label, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Helper.showRatingDialog(this, alertDialogCustom, levelNo);
        SharedPreferences.Editor prefs1 = getSharedPreferences("appOpenCount", Context.MODE_PRIVATE).edit();
        SharedPreferences prefs2 = getSharedPreferences("appOpenCount", Context.MODE_PRIVATE);
        prefs1.putInt("appOpenCount",prefs2.getInt("appOpenCount",0) + 1);
        prefs1.commit();
    }

    private void initializeData() {
        numCols = currentLevel.getColumns();
        numRows = currentLevel.getColumns();
        totalHintUsed = 0;
        addWords();
        wordHintUsed = new int[words.size()];
    }

    private void getIntentData() {
        /*currentPosition = getIntent().getIntExtra("position",0);
        stage = getIntent().getParcelableExtra("stage");
        if (getIntent().hasExtra("nextStage")) {
            nextStage = getIntent().getParcelableExtra("nextStage");
        }
        currentLevel = stage.getLevels().get(currentPosition);*/
    }

    private void initializeViews(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //TextView titleTV = (TextView) toolbar.findViewById(R.id.title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //titleTV.setText(stage.getName() + " : " + currentLevel.getName());
        ivChefCap = findViewById(R.id.iv_chef_cap);
        mContext = this;
        mPlayScreenLayout = (RelativeLayout) findViewById(R.id.playscreen_layout);
        lastTriedWord = (TextView) findViewById(R.id.lastTriedWord);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        SCREEN_WIDTH = displayMetrics.widthPixels;
        alertDialogCustom = (AlertDialogCustom) findViewById(R.id.alert_dialog_reward);
        actualDiamond = findViewById(R.id.actual_diamond);
        diamondCountTv = (TextView) findViewById(R.id.diamond_count_tv);
        SharedPreferences preferences = getSharedPreferences(NameConstant.GAME_DATABASE, MODE_PRIVATE);
        int diamondCount = preferences.getInt("guest_score",0);
        diamondCountTv.setText(String.valueOf(diamondCount));
        mpMusic = MediaPlayer.create(this, R.raw.game_music);
        if (Helper.isSoundEnabled(this)) {

            if (mpMusic != null) {
                mpMusic.setVolume(0.4f, 0.4f);
                mpMusic.setLooping(true);
                mpMusic.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        if (mp != null)
                            mp.release();
                    }
                });
                mpMusic.start();
            }
        }
    }

    private void addLettersOfPuzzle(){
        try {


            if (mPlayScreenLayout != null) {
                //add letter layouts in matrix form
                mPlayScreenLayout.removeAllViews();
                letterButtons.clear();
                lettersInitialCoordinates.clear();
                lettersMarked.clear();
                int newMarginBetweenLetters = MARGIN_BETWEEN_LETTERS / numCols;
                LETTER_WIDTH = (SCREEN_WIDTH / numCols) - (newMarginBetweenLetters * (numCols - 1));// - EXTRA_LEFT_AND_RIGHT_MARGIN * (numRows + 1);
                touchMargin = (int) (0.2 * LETTER_WIDTH);
                letterLeftMargin = (SCREEN_WIDTH - (LETTER_WIDTH * numCols) - (newMarginBetweenLetters * (numCols - 1))) / 2;
                int index = 1;
                char[] letters = getAllLetters(numCols * numRows);
                addEmptyLetters();
                lettersInitialCoordinates.clear();
                System.gc();
                BitmapDrawable letter_background = new BitmapDrawable(getResources(),BitmapFactory.decodeResource(getResources(),R.drawable.letter_background));

                for (int i = 0; i < numRows; i++) {
                    for (int j = 0; j < numCols; j++) {
                        final View letterLayout = getLayoutInflater().inflate(R.layout.letter_layout, null);
                        letterLayout.setId(index);
                        RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(
                                RelativeLayout.LayoutParams.WRAP_CONTENT,
                                RelativeLayout.LayoutParams.WRAP_CONTENT);
                        //ImageView button = (ImageView) letterLayout.findViewById(R.id.letterButton);
                        View letterInnerLayout = letterLayout.findViewById(R.id.letter_layout);
                        letterInnerLayout.setBackground(letter_background);
                        letterButtons.add(letterLayout);
                        Log.d("letter : ", String.valueOf(letters[index - 1]));
                        //button.setText(String.valueOf(letters[index - 1]));
                        //button.setImageResource(Helper.getLetterImageResID(letters[index - 1]));
                        letterInnerLayout.setTag(letters[index - 1]);
                        letterInnerLayout.setBackground(letter_background);
                        //button.setTextSize(TypedValue.COMPLEX_UNIT_DIP,(float) 0.15 * LETTER_WIDTH);
                        letterLayout.setTag(index);
                        LinearLayout.LayoutParams rlpButton = new LinearLayout.LayoutParams(LETTER_WIDTH, LETTER_WIDTH);
                        rlpButton.setMargins(LETTER_BUTTON_MARGIN, LETTER_BUTTON_MARGIN, LETTER_BUTTON_MARGIN, LETTER_BUTTON_MARGIN);
                        letterInnerLayout.setLayoutParams(rlpButton);
                        int padding = LETTER_WIDTH / 4;
                        letterInnerLayout.setPadding(padding * (3 / 4), padding / 2, padding * (3 / 4), padding);
                        //button.setTag(-1);
                        //if (j == 0) {
                        /*rlp.addRule(RelativeLayout.ALIGN_PARENT_LEFT,
                                RelativeLayout.TRUE);*/
                        int initialX = letterLeftMargin + LETTER_WIDTH * j + newMarginBetweenLetters * j - LETTER_BUTTON_MARGIN;
                        letterLayout.setX(initialX);
                        lettersInitialCoordinates.put(index + "x", String.valueOf(initialX));
                        int initialY = LETTER_WIDTH * i + newMarginBetweenLetters * i - LETTER_BUTTON_MARGIN;
                        letterLayout.setY(initialY);
                        lettersInitialCoordinates.put(index + "y", String.valueOf(initialY));
                        //} else {
                        //    rlp.addRule(RelativeLayout.RIGHT_OF, index - 1);
                        //}
                    /*if (i == 0) {
                        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP,
                                RelativeLayout.TRUE);
                    } else {
                        rlp.addRule(RelativeLayout.BELOW, index - NUM_COLS);
                    }*/
                        letterLayout.setLayoutParams(rlp);
                        mPlayScreenLayout.addView(letterLayout);

                        letterInnerLayout.setOnTouchListener(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View v, MotionEvent event) {
                                switch (event.getAction()) {
                                    case MotionEvent.ACTION_DOWN:
                                        lastTriedWord.setText("");
                                        //View imageLayout = v.findViewById(R.id.letterButton);
                                        //if ((int) imageLayout.getTag() >= 0) {
                                            setLetterMarked(v);
                                        //}
                                        break;
                                    case MotionEvent.ACTION_MOVE:
                                        View letter = getTouchedLetter(letterLayout.getX() + event.getX(),
                                                letterLayout.getY() + event.getY());
                                        if (letter != null) {
                                            setLetterMarked(letter);
                                        }
                                        break;
                                    case MotionEvent.ACTION_UP:
                                        LinearLayout container = getMatchedWordContainer();
                                        if (container != null) {
                                            View innerLayout = mPlayScreenLayout.findViewWithTag("inner_container" + (int) (Integer.parseInt((String) container.getTag()) / 2));
                                            moveLettersToEmptyLetterContainer(innerLayout, container);
                                        } else {
                                            shakeSelectedLetters();
                                            setAllLetterUnMarked();
                                        }
                                        break;
                                    default:
                                        return false;
                                }
                                return true;
                            }
                        });
                        index++;
                    }
                }
            }
        }catch (Exception ex){
            showProgressDialog("Loading");
            new Handler().postDelayed(new Runnable(){

                @Override
                public void run() {
                    hideProgressDialog();
                    addLettersOfPuzzle();
                }
            },2000);
        }
    }

    private View getTouchedLetter(float xTouch, float yTouch){
        /*if (touchMargin < 30){
            touchMargin = 48;
        }*/
        for (View button: letterButtons) {
            if (xTouch > button.getX() + touchMargin && xTouch < button.getX() + button.getWidth() - touchMargin
                    && yTouch > button.getY() + touchMargin && yTouch < button.getY() + button.getHeight() - touchMargin) {
                return button;
            }
        }
        return null;
    }

    private void addWords(){
        words.clear();
        if (currentLevel != null){
            if (!TextUtils.isEmpty(currentLevel.getWord1())){
                words.add(currentLevel.getWord1());
            }
            if (!TextUtils.isEmpty(currentLevel.getWord2())){
                words.add(currentLevel.getWord2());
            }
            if (!TextUtils.isEmpty(currentLevel.getWord3())){
                words.add(currentLevel.getWord3());
            }
            if (!TextUtils.isEmpty(currentLevel.getWord4())){
                words.add(currentLevel.getWord4());
            }
            if (!TextUtils.isEmpty(currentLevel.getWord5())){
                words.add(currentLevel.getWord5());
            }
        }
    }

    private char[] getAllLetters(int size){
        char[] letters = new char[size];
        int index = 0;
        for (int i = 0; i < currentLevel.getJumbledLetters().length(); i++){
            letters[index] = currentLevel.getJumbledLetters().charAt(i);
            index++;
        }
        return letters;
    }

    private void setLetterMarked(View view){
        //View button = view.findViewById(R.id.letterButton);
        View letterInnerLayout = view.findViewById(R.id.letter_layout);
        //if ((int)button.getTag() < 0) {
            letterInnerLayout.setBackgroundResource(R.drawable.letter_background_selected);
            //button.setTextColor(ContextCompat.getColor(mContext, R.color.selected_letter_color));
            lastTriedWord.setText(lastTriedWord.getText().toString() + letterInnerLayout.getTag());
            //button.setTag(lettersMarked.size());
            lettersMarked.add(view);
            if (Helper.isSoundEnabled(this)) {
                final MediaPlayer mp = MediaPlayer.create(this, R.raw.letter_selection_sound);
                if (mp != null) {
                    mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            if (mp != null) {
                                mp.release();
                            }
                        }
                    });
                    mp.start();
                }
            }
        //}
    }

    private void setAllLetterUnMarked(){
        for (View view: letterButtons) {
            //View button = view.findViewById(R.id.letterButton);
            View letterInnerLayout = view.findViewById(R.id.letter_layout);
            letterInnerLayout.setBackgroundResource(R.drawable.letter_background);
            //button.setTextColor(ContextCompat.getColor(mContext, R.color.default_letter_color));
            //button.setTag(-1);
        }
        lettersMarked.clear();
    }

    private void addEmptyLetters(){
        LinearLayout container = (LinearLayout) getLayoutInflater().inflate(R.layout.empty_words_outer_container_layout,null);
        RelativeLayout.LayoutParams rlpContainer = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        rlpContainer.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM,
                RelativeLayout.TRUE);
        rlpContainer.addRule(RelativeLayout.CENTER_HORIZONTAL,RelativeLayout.TRUE);
        RelativeLayout.LayoutParams rlpWordLayout = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        container.setLayoutParams(rlpContainer);
        emptyWordsContainer = container;
        mPlayScreenLayout.addView(container);
        for (int j = 0; j < words.size(); j++) {
            View emptyWordLayoutInnerContainer = (LinearLayout) getLayoutInflater().inflate(R.layout.empty_words_inner_container_layout, null);
            emptyWordLayoutInnerContainer.setLayoutParams(rlpWordLayout);
            emptyWordLayoutInnerContainer.setTag("inner_container" + (int)(j/2));
            LinearLayout emptyWordLayout1 = (LinearLayout) emptyWordLayoutInnerContainer.findViewById(R.id.empty_word_layout_1);
            emptyWordLayout1.setTag(String.valueOf(j));
            LinearLayout emptyWordLayout2 = (LinearLayout) emptyWordLayoutInnerContainer.findViewById(R.id.empty_word_layout_2);
            //emptyWordLayout.setTag(String.valueOf(j));
            container.addView(emptyWordLayoutInnerContainer);
            for (int k = 0; k < 2; k++) {
                for (int i = 0; i < words.get(j).length(); i++) {
                    final View letterLayout = getLayoutInflater().inflate(R.layout.empty_letter_layout, null);
                    letterLayout.setId(i);
                    RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(
                            RelativeLayout.LayoutParams.WRAP_CONTENT,
                            RelativeLayout.LayoutParams.WRAP_CONTENT);
                    letterLayout.setLayoutParams(rlp);
                    letterLayout.setTag(i);
                    if (k == 0) {
                        emptyWordLayout1.addView(letterLayout);
                    }else {
                        emptyWordLayout2.addView(letterLayout);
                    }
                }
                if (k == 0) {
                    j++;
                    if (j >= words.size()) {
                        break;
                    } else {
                        emptyWordLayout2.setTag(String.valueOf(j));
                    }
                }
            }
        }
    }

    private LinearLayout getMatchedWordContainer(){
        String createdWord = lastTriedWord.getText().toString();
        if (!TextUtils.isEmpty(createdWord)) {
            for (int i = 0; i < words.size(); i++) {
                if (createdWord.equalsIgnoreCase(words.get(i))) {
                    wordHintUsed[i] = words.get(i).length();
                    try {
                        String categoryName = "Game";
                        String label = stage.getName() + "_" + currentLevel.getName()
                                + "_" + words.get(i);
                        String action = "Word_Formed";
                        AnalyticsHelper.trackEvent(categoryName, action, label, this);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return (LinearLayout) mPlayScreenLayout.findViewWithTag(String.valueOf(i));
                }
            }
        }
        return null;
    }

    private void moveLettersToEmptyLetterContainer(View innerEmptyWordLayout, LinearLayout wordLayout){
        try {


            RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            android.os.Handler handler = new android.os.Handler();
            for (int i = 0; i < lettersMarked.size(); i++) {
                View view = lettersMarked.get(i);
                //Button button = (Button) view.findViewById(R.id.letterButton);
                View letterInnerLayout = view.findViewById(R.id.letter_layout);
                letterInnerLayout.setBackgroundResource(R.drawable.letter_background);
                final View replacedView = wordLayout.findViewWithTag(i);
                int width = replacedView.findViewById(R.id.letter_layout).getWidth();
                LinearLayout.LayoutParams rlpButton = new LinearLayout.LayoutParams(
                        width, width);
                //button.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);
                letterInnerLayout.setLayoutParams(rlpButton);
                int padding = width/5;
                letterInnerLayout.setPadding(padding,padding,padding,padding);
                view.setLayoutParams(rlp);

                float finalX = emptyWordsContainer.getX() + innerEmptyWordLayout.getX() + wordLayout.getX() + replacedView.getX();
                float finalY = emptyWordsContainer.getY() + innerEmptyWordLayout.getY() + wordLayout.getY() + replacedView.getY();
                removeLetter(view);
                handler.postDelayed(new Runnable() {
                    public void run() {
                        replacedView.setVisibility(View.INVISIBLE);
                    }
                }, 450);
                moveLetterToXAndY(view, finalX, finalY);
            }
            handler.postDelayed(new Runnable() {
                public void run() {
                    moveLetterRight();
                }
            }, 400);
            setAllLetterUnMarked();
            if (letterButtons != null && letterButtons.size() == 0) {
                handler.postDelayed(new Runnable() {
                    public void run() {
                        openLevelScoreDialog();
                    }
                }, 800);
            }
            if (Helper.isSoundEnabled(this)) {
                final MediaPlayer mp = MediaPlayer.create(this, R.raw.word_creation_sound);
                if (mp != null) {
                    mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            if (mp != null)
                                mp.release();
                        }
                    });
                    mp.start();
                }
            }
        }catch (Exception ex){
            try {
                String categoryName = "Exception";
                String label = stage.getName() + "_" + currentLevel.getName() + "_moveLettersToEmptyLetterContainer_function";
                String action = "";
                if (!TextUtils.isEmpty(ex.getLocalizedMessage())){
                    action += ex.getLocalizedMessage();
                }
                if (!TextUtils.isEmpty(ex.getMessage())){
                    action += ex.getMessage();
                }
                AnalyticsHelper.trackEvent(categoryName, action, label, this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void removeLetter(View view){
        letterButtons.remove(view);
        moveUpperLettersDown((int)view.getTag());
    }

    private void moveUpperLettersDown(int tag){
        for (View otherLetter : letterButtons){
            if (tag - numCols == (int)otherLetter.getTag()){
                moveUpperLettersDown((int) otherLetter.getTag());
                moveLetterToPossitionTag(otherLetter,tag);
            }
        }
        return;
    }

    private float getXForTagLetter(int tag){
        return Float.valueOf(lettersInitialCoordinates.get(tag + "x"));
    }

    private float getYForTagLetter(int tag){
        return Float.valueOf(lettersInitialCoordinates.get(tag + "y"));
    }

    private void moveLetterRight(){
        boolean isColEmpty;
        for (int i = 2; i < numCols; i++){
            isColEmpty = true;
            for (int j = 1; j <= numRows; j++){
                if (isLetterAvailableForMarking(getTagFromRowNoAndColNo(i,j))){
                    isColEmpty = false;
                    break;
                }
            }
            if (isColEmpty){
                fillBlankColumn(i);
            }
        }
    }

    private boolean isLetterAvailableForMarking(int tag){
        for (View letter : letterButtons){
            if (tag == (int)letter.getTag()){
                return true;
            }
        }
        return false;
    }

    private void fillBlankColumn(int colIndex){
        for (int i = 1; i <= numCols; i++){
            int initialTag = getTagFromRowNoAndColNo(colIndex - 1,i);
            int finalTag = getTagFromRowNoAndColNo(colIndex, i);
            View view = getLetterViewFromTag(initialTag);
            if (view != null) {
                moveLetterToPossitionTag(view, finalTag);
            }
        }

    }

    private View getLetterViewFromTag(int tag){
        for (View view : letterButtons){
            if ((int)view.getTag() == tag){
                return view;
            }
        }
        return null;
    }

    private void moveLetterToPossitionTag(View view,int tag){
        view.setTag(tag);
        moveLetterToXAndY(view,getXForTagLetter(tag),getYForTagLetter(tag));
    }

    private int getTagFromRowNoAndColNo(int col,int row){
        return numCols * (row - 1) + col;
    }

    private void moveLetterToXAndY(View view, float x, float y){
        view.animate()
                .x(x)
                .y(y)
                .setDuration(500)
                .start();
    }

    private void shakeSelectedLetters(){
        try {
            String categoryName = "Game";
            String label = stage.getName() + "_" + currentLevel.getName();
            String action = "Wrong_Word_Formed";
            AnalyticsHelper.trackEvent(categoryName, action, label, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            String categoryName = "Game";
            String label = stage.getName() + "_" + currentLevel.getName()
                    + "_" + lastTriedWord.getText();
            String action = "Wrong_Word_Formed_With_Wrong_Word";
            AnalyticsHelper.trackEvent(categoryName, action, label, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        final Animation animShake = AnimationUtils.loadAnimation(this, R.anim.shake_letter);
        for (View view : lettersMarked){
            //Button button = (Button) view.findViewById(R.id.letterButton);
            View letterInnerLayout = view.findViewById(R.id.letter_layout);
            letterInnerLayout.startAnimation(animShake);
        }
        if (Helper.isSoundEnabled(this)) {
            final MediaPlayer mp = MediaPlayer.create(this, R.raw.wrong_word_sound);
            if (mp != null) {
                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        if (mp != null)
                            mp.release();
                    }
                });
                mp.start();
            }
        }
    }

    private void openLevelScoreDialog() {
        try {
            String categoryName = "Game";
            String label = stage.getName() + "_" + currentLevel.getName();
            String action = "Completed";
            AnalyticsHelper.trackEvent(categoryName, action, label, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        alertDialogCustom.setResult(this, currentLevel);
        alertDialogCustom.showWithAnimation();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_SCORE_CARD && resultCode == RESULT_OK){
            currentPosition++;
            if (stage != null && stage.getLevels() != null){
                if (currentPosition < stage.getLevels().size()) {
                    //load next level
                    currentLevel = stage.getLevels().get(currentPosition);
                    loadLevel();
                } else {
                    //finish stage
                    setResult(RESULT_OK);
                    finish();
                    System.gc();
                }
            } else {
                //finish level
                finish();
                System.gc();
            }
        }
    }

    public void hintClicked(View view){
        //handle hint click
        try {
            String categoryName = "Hint";
            String label = stage.getName() + "_" + currentLevel.getName();
            String action = "Clicked";
            AnalyticsHelper.trackEvent(categoryName, action, label, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        SharedPreferences pref = getSharedPreferences(NameConstant.GAME_DATABASE, MODE_PRIVATE);
        int hint = pref.getInt("guest_score",0);
        if (hint >= 60) {

            int leastHint = wordHintUsed[0];
            int wordIndex = 0;
            for (int i = 1; i < words.size(); i++) {
                if (words.get(i).length() > wordHintUsed[i]) {
                    if (leastHint == words.get(wordIndex).length() || wordHintUsed[i] < leastHint) {
                        wordIndex = i;
                        leastHint = wordHintUsed[i];
                    }
                }
            }
            if (wordIndex == 0) {
                if (wordHintUsed[0] < words.get(0).length()) {
                    updateDiamondCount(-60);
                    showHintLetter(wordIndex);
                } else {
                    //all hint used

                }
            } else {
                updateDiamondCount(-60);
                showHintLetter(wordIndex);
            }
        }else {
            alertDialogCustom.setResultToWatchVideoForReward(this);
            alertDialogCustom.showWithAnimation();
        }
    }

    public void showHintLetter(int wordIndex){
        char ch = words.get(wordIndex).charAt(wordHintUsed[wordIndex]);
        View wordLayout = mPlayScreenLayout.findViewWithTag(String.valueOf(wordIndex));
        View letterView = wordLayout.findViewWithTag(wordHintUsed[wordIndex]++);
        ImageView button = (ImageView) letterView.findViewById(R.id.empty_letter_button);
        letterView.findViewById(R.id.letter_layout).setBackgroundResource(R.drawable.letter_background);
        button.setImageResource(Helper.getLetterImageResID(ch));
        try {
            String categoryName = "Hint";
            String label = stage.getName() + "_" + currentLevel.getName();
            String action = "Used";
            AnalyticsHelper.trackEvent(categoryName, action, label, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (Helper.isSoundEnabled(this)) {
            final MediaPlayer mp = MediaPlayer.create(this, R.raw.letter_selection_sound);
            if (mp != null) {
                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        if (mp != null)
                            mp.release();
                    }
                });
                mp.start();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home)
        {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void clueClicked(View v){
        try {
            String categoryName = "Clue";
            String label = stage.getName() + "_" + currentLevel.getName();
            String action = "Clicked_Viewed";
            AnalyticsHelper.trackEvent(categoryName, action, label, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //open clue activity
        String clueString = "";
        if (!TextUtils.isEmpty(currentLevel.getWordHint1())){
            clueString += "\n1. " + currentLevel.getWordHint1() + "\n";
        }
        if (!TextUtils.isEmpty(currentLevel.getWordHint2())){
            clueString += "\n2. " + currentLevel.getWordHint2() + "\n";
        }
        if (!TextUtils.isEmpty(currentLevel.getWordHint3())){
            clueString += "\n3. " + currentLevel.getWordHint3() + "\n";
        }
        if (!TextUtils.isEmpty(currentLevel.getWordHint4())){
            clueString += "\n4. " + currentLevel.getWordHint4() + "\n";
        }
        if (!TextUtils.isEmpty(currentLevel.getWordHint5())){
            clueString += "\n5. " + currentLevel.getWordHint5() + "\n";
        }
        alertDialogCustom.setResultForSimpleDialog(this,"Clue!", clueString);
        alertDialogCustom.showWithAnimation();
    }

    @Override
    protected void onResume() {
        super.onResume();
        hideNavigationBar();
        if (adcolonyAd == null || adcolonyAd.isExpired()){
            if (listener != null && ad_options != null) {
                AdColony.requestInterstitial(ZONE_ID, listener, ad_options);
            }
        }
        if (mpMusic != null)
            mpMusic.start();
    }

    @Override
    public void onRewardedVideoAdLoaded() {
        hideProgressDialog();
        if (mAd.isLoaded()) {
            try {
                String categoryName = "Video";
                String label = stage.getName() + "_" + currentLevel.getName();
                String action = null;
                action = "Loaded_From_Video";
                AnalyticsHelper.trackEvent(categoryName, action, label, this);
            } catch (Exception e) {
                e.printStackTrace();
            }
            mAd.show();
        }
    }

    @Override
    public void onRewardedVideoAdOpened() {

    }

    @Override
    public void onRewardedVideoStarted() {

    }

    @Override
    public void onRewardedVideoAdClosed() {
        try {
            String categoryName = "Video";
            String label = stage.getName() + "_" + currentLevel.getName();
            String action = null;
                action = "Unfinished_From_Video";

            AnalyticsHelper.trackEvent(categoryName, action, label, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRewarded(RewardItem rewardItem) {
        try {
            String categoryName = "Hint";
            String label = "From_Video_" + stage.getName() + "_" + currentLevel.getName();
            String action = "Claimed";
            AnalyticsHelper.trackEvent(categoryName, action, label, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            String categoryName = "Video";
            String label = stage.getName() + "_" + currentLevel.getName();
            String action = null;
                action = "Completed_From_Video";

            AnalyticsHelper.trackEvent(categoryName, action, label, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        alertDialogCustom.setResult(this, 60);
        alertDialogCustom.showWithAnimation();
    }

    @Override
    public void onRewardedVideoAdLeftApplication() {

    }

    @Override
    public void onRewardedVideoAdFailedToLoad(int i) {
        hideProgressDialog();
        alertDialogCustom.setResultForSimpleDialog(this,"Error loading video", null);
        alertDialogCustom.showWithAnimation();
        try {
            String categoryName = "Video";
            String label = stage.getName() + "_" + currentLevel.getName();
            String action = null;

                action = "Error_From_Video_Button" + i;

            AnalyticsHelper.trackEvent(categoryName, action, label, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRewardedVideoCompleted() {

    }

    private void loadRewardedVideoAd() {
        if (adcolonyAd != null && !adcolonyAd.isExpired()){
            adcolonyAd.show();
        }else {
            AdColony.requestInterstitial( ZONE_ID, listener, ad_options );
            loadAdmobRewardedVideo();
        }
    }

    private void showProgressDialog(String message){
        RelativeLayout progressDialogLayout = (RelativeLayout) findViewById(R.id.progress_dialog);
        TextView progressDialogTV = (TextView) findViewById(R.id.progress_dialog_text);
        progressDialogTV.setText(message);
        progressDialogLayout.setVisibility(View.VISIBLE);
    }

    private void hideProgressDialog(){
        RelativeLayout progressDialogLayout = (RelativeLayout) findViewById(R.id.progress_dialog);
        progressDialogLayout.setVisibility(View.GONE);
    }

    private void hideNavigationBar(){
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    @Override
    public void onBackPressed() {
        hideProgressDialog();
        if (mAd != null) {
            mAd.destroy(this);
        }
        if (alertDialogCustom.getVisibility() == View.VISIBLE){
            alertDialogCustom.hideWithAnimation();
            return;
        }
        if (isCustomProgressDialogVisible()){
            hideProgressDialog();
            return;
        }
        super.onBackPressed();
    }

    private boolean isCustomAlertDialogVisible(){
        RelativeLayout alertDialogLayout = (RelativeLayout) findViewById(R.id.alert_dialog);
        if (alertDialogLayout.getVisibility() == View.VISIBLE){
            return true;
        }else {
            return false;
        }
    }

    private boolean isCustomProgressDialogVisible(){
        RelativeLayout progressDialogLayout = (RelativeLayout) findViewById(R.id.progress_dialog);
        if (progressDialogLayout.getVisibility() == View.VISIBLE){
            return true;
        }else {
            return false;
        }
    }

    private void loadAdmobRewardedVideo(){
        mAd.loadAd("ca-app-pub-2649338194133294/8258689979", new AdRequest.Builder().build());
        showProgressDialog("Loading Video");
        try {
            String categoryName = "Video";
            String label = stage.getName() + "_" + currentLevel.getName();
            String action = null;
            action = "Played_From_Video";
            AnalyticsHelper.trackEvent(categoryName, action, label, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initAdcolony(){
        /** Construct optional app options object to be sent with configure */
        AdColonyAppOptions app_options = new AdColonyAppOptions();

        /**
         * Configure AdColony in your launching Activity's onCreate() method so that cached ads can
         * be available as soon as possible.
         */
        AdColony.configure( this, app_options, APP_ID, ZONE_ID );

        /** Ad specific options to be sent with request */
        ad_options = new AdColonyAdOptions()
                .enableConfirmationDialog( false )
                .enableResultsDialog( false );

        /** Create and set a reward listener */
        final Activity activity = this;
        AdColony.setRewardListener( new AdColonyRewardListener()
        {
            @Override
            public void onReward( AdColonyReward reward )
            {
                /** Query reward object for info here */
                Log.d( TAG, "onReward" );
                try {
                    String categoryName = "Hint";
                    String label = "From_Video_" + stage.getName() + "_" + currentLevel.getName()+ "_adColony";
                    String action = "Claimed";
                    AnalyticsHelper.trackEvent(categoryName, action, label, mActivity);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    String categoryName = "Video";
                    String label = stage.getName() + "_" + currentLevel.getName();
                    String action = null;
                    action = "Completed_From_Video_Button_adColony";
                    AnalyticsHelper.trackEvent(categoryName, action, label, mActivity);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                alertDialogCustom.setResult(activity, 60);
                alertDialogCustom.showWithAnimation();
            }
        } );

        /**
         * Set up listener for interstitial ad callbacks. You only need to implement the callbacks
         * that you care about. The only required callback is onRequestFilled, as this is the only
         * way to get an ad object.
         */
        listener = new AdColonyInterstitialListener()
        {
            /** Ad passed back in request filled callback, ad can now be shown */
            @Override
            public void onRequestFilled( AdColonyInterstitial ad )
            {
                adcolonyAd = ad;
                Log.d( TAG, "onRequestFilled" );
            }

            /** Ad request was not filled */
            @Override
            public void onRequestNotFilled( AdColonyZone zone )
            {
                Log.d( TAG, "onRequestNotFilled");
            }

            /** Ad opened, reset UI to reflect state change */
            @Override
            public void onOpened( AdColonyInterstitial ad )
            {
                Log.d( TAG, "onOpened" );
                try {
                    String categoryName = "Video";
                    String label = stage.getName() + "_" + currentLevel.getName();
                    String action = null;
                        action = "Played_From_Hint_Dialog_Button_adColony";
                    AnalyticsHelper.trackEvent(categoryName, action, label, mActivity);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            /** Request a new ad if ad is expiring */
            @Override
            public void onExpiring( AdColonyInterstitial ad )
            {
                AdColony.requestInterstitial( ZONE_ID, this, ad_options );
                Log.d( TAG, "onExpiring" );
            }
        };
    }

    @Override
    public void loadNewStage() {
        currentLevel.setLevelCompleted(true,this);
        if (stage != null){
            if (stage.getLastLevelUnlocked() == null){
                stage.setCompleted(true,this);
            }
        }
        loadLevel();
    }

    @Override
    public void loadVideo() {
        loadRewardedVideoAd();
    }

    @Override
    public void animateDiamond(final int diamondEarned) {
        actualDiamond.animate()
                .scaleXBy(1)
                .scaleYBy(1)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        actualDiamond.animate()
                                .scaleXBy(-1)
                                .scaleYBy(-1)
                                .setListener(null)
                                .setDuration(300);
                        updateDiamondCount(diamondEarned);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .setDuration(800);
    }

    @Override
    public void wordFormedInCanvas(String word) {

    }

    @Override
    public void onLetterSelectedInCanvas(String word) {

    }

    private void updateDiamondCount(int diamondEarned){
        //TODO play sound
        SharedPreferences preferences = getSharedPreferences(NameConstant.GAME_DATABASE, MODE_PRIVATE);
        SharedPreferences.Editor editor = getSharedPreferences(NameConstant.GAME_DATABASE, MODE_PRIVATE).edit();
        int score = preferences.getInt("guest_score",0);
        score = score + diamondEarned;
        editor.putInt("guest_score",score);
        editor.apply();
        if (Helper.isSoundEnabled(this)) {
            final MediaPlayer mp = MediaPlayer.create(this, R.raw.diamond_earned_sound);
            if (mp != null) {
                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        if (mp != null)
                            mp.release();
                    }
                });
                mp.start();
            }
        }
        diamondCountTv.setText(String.valueOf(score));

    }

    public void onVideoButtonClicked(View v){
        alertDialogCustom.setResultToWatchVideoForReward(this);
        alertDialogCustom.showWithAnimation();
    }

    public void onHomeClick(View v){
        onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mpMusic != null)
            mpMusic.pause();
    }

    public void startSpinWheel(View view) {
    }

    public void onSpinCloseClicked(View view) {
    }

    public void onSpinClaimClick(View view) {
    }
}
