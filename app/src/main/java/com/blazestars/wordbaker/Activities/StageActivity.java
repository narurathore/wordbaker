package com.blazestars.wordbaker.Activities;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.blazestars.wordbaker.Adapters.StageAdapter;
import com.blazestars.wordbaker.Helper.AnalyticsHelper;
import com.blazestars.wordbaker.Modal.GameData;
import com.blazestars.wordbaker.Modal.NameConstant;
import com.blazestars.wordbaker.Modal.Stage;
import com.blazestars.wordbaker.Network.VolleyData;
import com.blazestars.wordbaker.R;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;

public class StageActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    int REQUEST_CODE_LEVEL_LIST = 1;
    RecyclerView recyclerView;
    StageAdapter stageAdapter;
    ProgressBar progressBar;
    GameData gameData;
    Context mContext;
    int prevStagePosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stage);
        try {
            final TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            final String simCountry = tm.getSimCountryIso();
            if (TextUtils.isEmpty(simCountry)) {
                FirebaseMessaging.getInstance().subscribeToTopic("notification_word_baker");
            }else {
                Log.v("country : ", simCountry);
                FirebaseMessaging.getInstance().subscribeToTopic("notification_word_baker_" + simCountry);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        mContext = this;
        initiateUIComponents();
        loadStages();
        SharedPreferences pref = getSharedPreferences(NameConstant.GAME_DATABASE, MODE_PRIVATE);
        boolean isFirstLaunch = pref.getBoolean("is_first_launch",true);
        if (isFirstLaunch){
            SharedPreferences.Editor editor = getSharedPreferences(NameConstant.GAME_DATABASE, MODE_PRIVATE).edit();
            editor.putInt("hint", 3);
            editor.putBoolean("is_first_launch",false);
            editor.apply();
            openHowToPlayActivity();
        }
        try {
            String pageviews = "Home";
            AnalyticsHelper.trackPageView(pageviews, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    StageAdapter.OnItemClickListener stageItemClickListener = new StageAdapter.OnItemClickListener() {
        @Override
        public void onItemClick(View view, int position) {
            openStage(position,false,0);
        }
    };

    private void initiateUIComponents(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        //NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        //navigationView.setNavigationItemSelectedListener(this);
        //getSupportActionBar().setTitle("Word Baker");
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        recyclerView = (RecyclerView) findViewById(R.id.stage_recycler_view);
        stageAdapter = new StageAdapter(this, new ArrayList<Stage>());
        stageAdapter.setOnItemClickListener(stageItemClickListener);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(stageAdapter);
        setScoreInMenu();
        if (getIntent().getBooleanExtra("isHint",false)
                || "OPEN_APP_WITH_HINT".equalsIgnoreCase(getIntent().getStringExtra("notification_type"))){
            updateHint(2);
            showCustomAlertDialog("Congratulations!","You got Today's 2 hints");
            try {
                String categoryName = "Hint";
                String label = "From Notification";
                String action = "Claimed";
                AnalyticsHelper.trackEvent(categoryName, action, label, this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void loadStages(){
        recyclerView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        new VolleyData(this){

            @Override
            protected void VPreExecute() {
                recyclerView.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected void VResponse(JSONObject response, String tag) {
                recyclerView.setVisibility(View.VISIBLE);
                gameData = new Gson().fromJson(response.toString(),GameData.class);
                if (gameData != null){
                    stageAdapter = new StageAdapter(mContext,gameData.getStages());
                    if (gameData.getStages() != null && gameData.getStages().size() > 0){
                        gameData.getStages().get(0).openStage(mContext);
                    }
                    gameData.setLockUnlockedStateFromSharedPref(mContext);
                    stageAdapter.setOnItemClickListener(stageItemClickListener);
                    recyclerView.setAdapter(stageAdapter);
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            protected void VError(VolleyError error, String tag) {
                recyclerView.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
            }
        }.getJsonObject(NameConstant.GET_GAME_DATA_URL,"Game Data",this,true);

        //will refersh cache
        new VolleyData(this){
            @Override
            protected void VPreExecute() {
            }

            @Override
            protected void VResponse(JSONObject response, String tag) {

            }

            @Override
            protected void VError(VolleyError error, String tag) {

            }
        }.getJsonObject(NameConstant.GET_GAME_DATA_URL,"Game Data Cache",this,false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_LEVEL_LIST && resultCode == RESULT_OK){
            int nextStagePosition = prevStagePosition + 1;
            gameData.getStages().get(prevStagePosition).setCompleted(true,this);
            if (gameData != null && gameData.getStages() != null
                    && nextStagePosition < gameData.getStages().size()){
                gameData.getStages().get(nextStagePosition).openStage(mContext);
                openStage(nextStagePosition,true,0);
            }
            gameData.setLockUnlockedStateFromSharedPref(this);
            stageAdapter.notifyDataSetChanged();
        }
    }

    private void openStage(int position,boolean openGame,int levelNumber){
        Intent intent = new Intent(mContext,LevelActivity.class);
        intent.putExtra("stage",gameData.getStages().get(position));
        if (gameData.getStages().size() > position + 1){
            intent.putExtra("nextStage",gameData.getStages().get(position + 1));
        }
        intent.putExtra("openGame",openGame);
        intent.putExtra("levelNumber",levelNumber);
        prevStagePosition = position;
        startActivityForResult(intent,REQUEST_CODE_LEVEL_LIST);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        /*// Handle navigation view item clicks here.
        int id = item.getItemId();

        *//*if (id == R.id.score_card) {

        } else*//* if (id == R.id.nav_share) {
            try{
                String title = "Play Word Baker puzzle game, to download click the below link :";
                String appLink = "\n http://play.google.com/store/apps/details?id=" + getPackageName()+"\n";
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, title + appLink);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
                *//*try {
                    String categoryName = "Story Share Dialog Opened";
                    String label = story.title;
                    String action2 = story.postId;
                    AnalyticsHelper.trackEvent(categoryName, action2, label, (Activity) context);
                } catch (Exception e) {
                    e.printStackTrace();
                }*//*
            }catch (Exception ex){

            }
        } else if (id == R.id.how_to_play) {
            openHowToPlayActivity();
        }else if (id == R.id.feedback) {
            Intent Email = new Intent(Intent.ACTION_SEND);
            Email.setType("text/email");
            Email.putExtra(Intent.EXTRA_EMAIL, new String[]{"feedbackwordbaker@gmail.com"});
            Email.putExtra(Intent.EXTRA_SUBJECT, "Feedback For "+ getString(R.string.app_name)+" android application");
            //AnalyticsHelper.MobileInfo mobileInfo = new AnalyticsHelper.MobileInfo(context);
            //Email.putExtra(Intent.EXTRA_TEXT, "\tDevice Info :\n\tModel Name : " + mobileInfo.getMobileName() + "\n\tAndroid OS Version : " + mobileInfo.getMobileOs()+ "\n\tMobile Resolution : " + mobileInfo.getMobileResolution()+ "\n\tManufacturer : " + mobileInfo.getManufacturer()+ "\n\tApplication Version : " + mobileInfo.getappVersionName()+"\n\n");
            startActivity(Intent.createChooser(Email, "Send Feedback:"));
        } else if (id == R.id.rate_us) {
            Uri uri = Uri.parse("market://details?id=" + getPackageName());
            Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);

            goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                    Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET |
                    Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
            try {
                startActivity(goToMarket);
            } catch (ActivityNotFoundException e) {
                startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://play.google.com/store/apps/details?id=" + getPackageName())));
            }
        } else if (id == R.id.about_us) {
            Intent intent = new Intent(this,AboutUs.class);
            intent.putExtra("header","aboutus");
            startActivity(intent);
        }else if (id == R.id.settings) {
            Intent intent = new Intent(this,SettingsActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);*/
        return true;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (isCustomAlertDialogVisible()){
            hideCustomAlertDialog();
            return;
        }
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        setScoreInMenu();
    }

    private void setScoreInMenu(){
        try {
            SharedPreferences preferences = getSharedPreferences(NameConstant.GAME_DATABASE,MODE_PRIVATE);
            int score = preferences.getInt("guest_score",0);
            TextView scoreTV = (TextView) findViewById(R.id.score);
            scoreTV.setText("Score : " + score);
        }catch (Exception ex){

        }
    }

    private void showCustomAlertDialog(String title, String message){
        RelativeLayout alertDialogLayout = (RelativeLayout) findViewById(R.id.alert_dialog);
        TextView titleTV = (TextView) alertDialogLayout.findViewById(R.id.title);
        TextView messageTV = (TextView) alertDialogLayout.findViewById(R.id.message);
        TextView okayTV = (TextView) alertDialogLayout.findViewById(R.id.okay_text_view);
        TextView cancelTV = (TextView) alertDialogLayout.findViewById(R.id.cancel_text_view);
        View cancelDivider = alertDialogLayout.findViewById(R.id.cancel_divider);
        titleTV.setText(title);
        messageTV.setText(message);
        alertDialogLayout.setVisibility(View.VISIBLE);
        cancelDivider.setVisibility(View.GONE);
        cancelTV.setVisibility(View.GONE);
        okayTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideCustomAlertDialog();
            }
        });
        alertDialogLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void hideCustomAlertDialog(){
        RelativeLayout alertDialogLayout = (RelativeLayout) findViewById(R.id.alert_dialog);
        alertDialogLayout.setVisibility(View.GONE);
    }

    private boolean isCustomAlertDialogVisible(){
        RelativeLayout alertDialogLayout = (RelativeLayout) findViewById(R.id.alert_dialog);
        if (alertDialogLayout.getVisibility() == View.VISIBLE){
            return true;
        }else {
            return false;
        }
    }

    private void updateHint(int hintsAddedOrRemoved){
        SharedPreferences pref = getSharedPreferences(NameConstant.GAME_DATABASE, MODE_PRIVATE);
        int hint = pref.getInt("hint",0) + hintsAddedOrRemoved;
        SharedPreferences.Editor editor = getSharedPreferences(NameConstant.GAME_DATABASE, MODE_PRIVATE).edit();
        editor.putInt("hint", hint);
        editor.apply();
    }

    private void openHowToPlayActivity(){
        Intent intent = new Intent(this,HowToPlayActivity.class);
        startActivity(intent);
    }
}
