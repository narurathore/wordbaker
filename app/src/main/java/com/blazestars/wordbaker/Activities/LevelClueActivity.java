package com.blazestars.wordbaker.Activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.blazestars.wordbaker.Helper.AnalyticsHelper;
import com.blazestars.wordbaker.R;

public class LevelClueActivity extends AppCompatActivity {

    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level_clue);
        mContext = this;
        String clueString = getIntent().getStringExtra("clueString");
        TextView clueStringTV = (TextView) findViewById(R.id.clue_text);
        clueStringTV.setText(clueString);
        try {
            String pageviews = "Clue";
            AnalyticsHelper.trackPageView(pageviews, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void continueClicked(View v){
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }
}
