package com.blazestars.wordbaker.Interfaces;

/**
 * Created by narayansingh on 11/05/18.
 */

public interface GameScreenListner {
    void loadNewStage();
    void loadVideo();
    void animateDiamond(int diamondEarned);
    void wordFormedInCanvas(String word);
    void onLetterSelectedInCanvas(String word);
}
