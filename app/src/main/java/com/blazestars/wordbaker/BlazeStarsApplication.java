package com.blazestars.wordbaker;

import android.support.multidex.MultiDexApplication;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.google.firebase.FirebaseApp;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;


public class BlazeStarsApplication extends MultiDexApplication {

	private static BlazeStarsApplication ourApplication;/**< App Instance*/

	


	
	public static BlazeStarsApplication Instance() {
		return ourApplication;
	}
	public BlazeStarsApplication() {
		ourApplication = this;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		Fabric.with(this, new Crashlytics());
		FirebaseApp.initializeApp(getApplicationContext());
		Fresco.initialize(this);
	}
	@Override
	public void onTerminate() {
		super.onTerminate();
	}



}