package com.blazestars.wordbaker.Adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.blazestars.wordbaker.Modal.Stage;
import com.blazestars.wordbaker.R;
//import com.joanzapata.android.iconify.IconDrawable;
//import com.joanzapata.android.iconify.Iconify;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by narayansingh on 06/08/17.
 */

public class StageAdapter extends RecyclerView.Adapter<StageAdapter.CustomViewHolder> {

    private Context mContext;
    List<Stage> stages;
    OnItemClickListener mItemClickListener;

    public StageAdapter(Context context, List<Stage> stages) {
        mContext = context;
        this.stages = stages;
    }

    @Override
    public int getItemCount() {
        return stages.size();
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.stage_or_level_recycler_item, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        Stage stage = stages.get(position);
        holder.stageTitle.setText(stage.getName());
        holder.stagePoints.setText("Points : " + stage.getPoints());
        if (stage.isLocked()){
            /*Drawable lock = new IconDrawable(mContext, Iconify.IconValue.fa_lock)
                    .colorRes(R.color.cream_color)
                    .actionBarSize();
            holder.lockImage.setImageDrawable(lock);*/
            holder.translucentView.setVisibility(View.VISIBLE);
        }else {
            /*Drawable rightArrow = new IconDrawable(mContext, Iconify.IconValue.fa_chevron_right)
                    .colorRes(R.color.cream_color)
                    .actionBarSize();
            holder.lockImage.setImageDrawable(rightArrow);*/
            holder.translucentView.setVisibility(View.INVISIBLE);
        }
        if (stage.isCompleted()){
            holder.completedImage.setVisibility(View.VISIBLE);
        }else {
            holder.completedImage.setVisibility(View.GONE);
        }
        Picasso.with(mContext).load(stage.getImage()).into(holder.stageImage);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView stageTitle,stagePoints;
        ImageView stageImage,lockImage, completedImage;
        View translucentView;

        public CustomViewHolder(View itemView) {
            super(itemView);
            stageTitle = (TextView) itemView.findViewById(R.id.stage_title);
            stagePoints = (TextView) itemView.findViewById(R.id.stage_points);
            stageImage = (ImageView) itemView.findViewById(R.id.stage_image);
            lockImage = (ImageView) itemView.findViewById(R.id.lock_image);
            completedImage = (ImageView) itemView.findViewById(R.id.completed_image);
            translucentView = itemView.findViewById(R.id.translucent_layout);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null && !stages.get(getPosition()).isLocked()) {
                mItemClickListener.onItemClick(v,getPosition());
            }
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

}
