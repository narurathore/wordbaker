package com.blazestars.wordbaker.CustomViews;

import android.animation.Animator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.blazestars.wordbaker.Helper.Helper;
import com.blazestars.wordbaker.R;
import com.facebook.drawee.drawable.ScalingUtils;
import com.facebook.drawee.view.SimpleDraweeView;

/**
 * Created by narayansingh on 10/05/18.
 */

public abstract class AlertDialogBase extends RelativeLayout {

    protected RelativeLayout alertDialogContentLayout;

    public AlertDialogBase(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.alert_dailog_base, this, true);
        SimpleDraweeView simpleDraweeView = findViewById(R.id.my_image_view);
        simpleDraweeView.getHierarchy().setActualImageScaleType(ScalingUtils.ScaleType.FIT_XY);
        simpleDraweeView.setImageURI(Helper.
                getURIForResourceImage(R.drawable.dialog_background));

        alertDialogContentLayout = (RelativeLayout) findViewById(R.id.alert_dialog_content);
        setVisibility(GONE);
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }


    public void hideWithAnimation(){
        alertDialogContentLayout.animate()
                .translationY(-alertDialogContentLayout.getHeight())
                .setInterpolator(new AccelerateInterpolator())
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        setVisibility(GONE);
                        alertDialogContentLayout.setY(- 1000);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .setDuration(500);
    }

    public void showWithAnimation(){
        setVisibility(VISIBLE);
        alertDialogContentLayout.animate()
                .translationY((getY() + getHeight()/2 - alertDialogContentLayout.getHeight()) / 2)
                .setInterpolator(new DecelerateInterpolator())
                .setListener(null)
                .setDuration(500);
    }
}
