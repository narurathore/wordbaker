package com.blazestars.wordbaker.CustomViews;

import android.animation.Animator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.CornerPathEffect;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.blazestars.wordbaker.Interfaces.GameScreenListner;
import com.blazestars.wordbaker.Modal.NameConstant;
import com.blazestars.wordbaker.Modal.WordConnectLevel;
import com.blazestars.wordbaker.Modal.WordConnectLevelLetter;
import com.blazestars.wordbaker.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Handler;

/**
 * Created by narayansingh on 17/05/18.
 */

public class CanvasView extends View {

    View parentViewOfButtons;
    List<View> views;
    GameScreenListner gameListner;
    List<View> selectedViews = new ArrayList<>();
    float radiusX = 220f;
    float radiusY = 220f;
    float lastX;
    float lastY;
    float parentViewX;
    float parentViewY;
    float centerX;
    float centerY;
    int strokeColorResourse;
    WordConnectLevel currentLevel;
    View hand;
    boolean isAnimating = false;
    private Paint paint;
    //private Paint paint2;

    public CanvasView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        strokeColorResourse = R.color.cream_color;
        paint = new Paint();
        paint.setColor(getResources().getColor(strokeColorResourse));                    // set the color
        paint.setStrokeWidth(30);               // set the size
        paint.setDither(true);                    // set the dither to true
        paint.setStyle(Paint.Style.STROKE);       // set to STOKE
        paint.setStrokeJoin(Paint.Join.ROUND);    // set the join to round you want
        paint.setStrokeCap(Paint.Cap.ROUND);      // set the paint cap to round too
        paint.setPathEffect(new CornerPathEffect(50) );// set the path effect when they join.
        paint.setAntiAlias(true);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        Path path = new Path();
        if (selectedViews.size() > 0) {
            path.moveTo(parentViewX + selectedViews.get(0).getX() + selectedViews.get(0).getWidth()/2,
                    parentViewY + selectedViews.get(0).getY() + selectedViews.get(0).getHeight()/2);
            for (int i=1; i < selectedViews.size(); i++){
                path.lineTo(parentViewX + selectedViews.get(i).getX()+ selectedViews.get(i).getWidth()/2,
                        parentViewY + selectedViews.get(i).getY()+ selectedViews.get(i).getHeight()/2);
            }
            path.lineTo(lastX, lastY);
            canvas.drawPath(path, paint);
        }
        drawFisrtStageHowToPlay(canvas);
    }

    private void drawFisrtStageHowToPlay(Canvas canvas) {
        if (currentLevel != null && currentLevel.getLevelNo() == 1) {
            Paint paint = new Paint();
            paint.setColor(getResources().getColor(strokeColorResourse));                    // set the color
            paint.setStrokeWidth(30);               // set the size
            paint.setDither(true);                    // set the dither to true
            paint.setStyle(Paint.Style.STROKE);       // set to STOKE
            paint.setStrokeJoin(Paint.Join.ROUND);    // set the join to round you want
            paint.setStrokeCap(Paint.Cap.ROUND);      // set the paint cap to round too
            paint.setAntiAlias(true);
            paint.setPathEffect(new DashPathEffect(new float[]{30, 40}, 30));
            Path path = new Path();
            String word = currentLevel.getNextUnformedWord();
            if (word != null) {
                final List<View> letterOfWords = getLetterOfWordsFromCanvas(word);
                if (letterOfWords != null && letterOfWords.size() > 0) {
                    if (letterOfWords.size() > 0) {
                        path.moveTo(parentViewX + letterOfWords.get(0).getX() + letterOfWords.get(0).getWidth() / 2,
                                parentViewY + letterOfWords.get(0).getY() + letterOfWords.get(0).getHeight() / 2);
                        for (int i = 1; i < letterOfWords.size(); i++) {
                            path.lineTo(parentViewX + letterOfWords.get(i).getX() + letterOfWords.get(i).getWidth() / 2,
                                    parentViewY + letterOfWords.get(i).getY() + letterOfWords.get(i).getHeight() / 2);
                        }
                        //path.lineTo(lastX, lastY);
                        canvas.drawPath(path, paint);
                        if (hand != null && !isAnimating){
                            isAnimating = true;
                            hand.setX(parentViewX + letterOfWords.get(0).getX());
                            hand.setY(parentViewY + letterOfWords.get(0).getY() + letterOfWords.get(0).getWidth() / 2);
                            hand.setVisibility(VISIBLE);
                            android.os.Handler handler = new android.os.Handler();
                            for (int i = 1; i < letterOfWords.size(); i++){
                                final int index = i;
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        hand.animate()
                                                .x(parentViewX + letterOfWords.get(index).getX())
                                                .y(parentViewY + letterOfWords.get(index).getY() + letterOfWords.get(0).getWidth() / 2)
                                                .setListener(new Animator.AnimatorListener() {
                                                    @Override
                                                    public void onAnimationStart(Animator animator) {

                                                    }

                                                    @Override
                                                    public void onAnimationEnd(Animator animator) {
                                                        if (hand != null && index == letterOfWords.size() - 1){
                                                            hand.setVisibility(GONE);
                                                            isAnimating = false;
                                                        }
                                                    }

                                                    @Override
                                                    public void onAnimationCancel(Animator animator) {

                                                    }

                                                    @Override
                                                    public void onAnimationRepeat(Animator animator) {

                                                    }
                                                })
                                                .setDuration(1000);
                                    }
                                },(i-1)*1000);
                            }
                        }
                    }
                }
            }
        }
    }

    private List<View> getLetterOfWordsFromCanvas(String word){
        if (word != null && views != null) {
            List<View> letterOfWords = new ArrayList<>();
            for (int i = 0; i < word.length(); i++){
                String letter = String.valueOf(word.charAt(i));
                for (View view : views){
                    String valueInViewview = String.valueOf((char)view.findViewById(R.id.letter_layout).getTag());
                    if (letter.equalsIgnoreCase(valueInViewview)){
                        letterOfWords.add(view);
                    }
                }
            }
            return letterOfWords;
        }
        return null;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_MOVE:
                    View touchedView = getTouchedView(event.getX(), event.getY());
                    if (touchedView != null && !selectedViews.contains(touchedView)) {
                        selectedViews.add(touchedView);
                        gameListner.onLetterSelectedInCanvas(getWordFormedInCanvas());
                    }
                    lastX = event.getX();
                    lastY = event.getY();
                    invalidate();
                    break;
                case MotionEvent.ACTION_DOWN:
                    clearCanvas();
                    break;
                case MotionEvent.ACTION_UP:
                    if (gameListner != null && selectedViews.size() > 0) {
                        gameListner.wordFormedInCanvas(getWordFormedInCanvas());
                    }
                    break;
                default:
                    View touchedViews = getTouchedView(event.getX(), event.getY());
                    if (touchedViews != null) {
                        selectedViews.add(touchedViews);
                    }
                    lastX = event.getX();
                    lastY = event.getY();
                    invalidate();
                    break;
            }
        return true;
    }

    public void setLettersInCanvas(List<View> views){
        this.views = views;
    }

    public void setGameListner(GameScreenListner gameListner) {
        this.gameListner = gameListner;
    }

    public View getTouchedView(float x, float y){
        if (parentViewOfButtons != null) {
            float parentX = parentViewOfButtons.getX();
            float parentY = parentViewOfButtons.getY();
            for (View view : views) {
                if (x > parentX + view.getX() && x < parentX + view.getX() + view.getWidth()
                        && y > parentY + view.getY() && y < parentY + view.getY() + view.getHeight()) {
                    return view;
                }
            }
        }
        return null;
    }

    public void setParentViewOfButtons(View parentViewOfButtons) {
        this.parentViewOfButtons = parentViewOfButtons;
    }

    public void arrangeButtons(){
        float angleDiffernce = (float) ((360/views.size()));
        angleDiffernce = (float) Math.toRadians(angleDiffernce);
        parentViewX = parentViewOfButtons.getX();
        parentViewY = parentViewOfButtons.getY();
        float pointAngle = -(float) Math.PI/2;
        centerX = parentViewOfButtons.getWidth()/2 - 75;
        centerY = parentViewOfButtons.getHeight()/2 - 40;
        if (views.size() > 5){
            radiusX = 320f;
            radiusY = 230f;
        }
        double radiusForLetter = Math.sqrt(((5*(views.get(0).getWidth())^2)/4));
        for (View view: views){
            float x = centerX + radiusX*(float) Math.cos(pointAngle);
            float y = centerY + radiusY*(float) Math.sin(pointAngle);
            x = (float) (x + (radiusForLetter * Math.cos(-3*Math.PI/4)));
            y = (float) (y + (radiusForLetter * Math.sin(-3*Math.PI/4)));
            view.animate()
                    .x(x)
                    .y(y)
                    .setDuration(500)
                    .start();
            //view.setX(centerX + radius*(float) Math.cos(pointAngle));
            //view.setY(centerY + radius*(float) Math.sin(pointAngle));
            pointAngle += angleDiffernce;
        }
        if (hand != null){
            hand.setVisibility(GONE);
        }
        new android.os.Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                invalidate();
                if (currentLevel != null && currentLevel.getLevelNo() == 1){
                    String word = currentLevel.getNextUnformedWord();
                    if (!TextUtils.isEmpty(word)){
                        postDelayed(this, 3000);
                    }
                }
            }
        },2000);
    }

    private String getWordFormedInCanvas(){
        String wordFormed = "";
        for (View view : selectedViews) {
            char letter = (char)view.findViewById(R.id.letter_layout).getTag();
            wordFormed += letter;
        }
        return wordFormed;
    }

    public void resetCanvas(){
        if (views != null) {
            views.clear();
        }
        if (selectedViews != null) {
            selectedViews.clear();
        }
    }

    public void shuffleLetters(){
        Collections.shuffle(views);
        for (View view: views){
            view.animate()
                    .x(centerX)
                    .y(centerY)
                    .setDuration(500)
                    .start();
        }
        new android.os.Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                arrangeButtons();
            }
        },400);
    }

    public void updateStatusAfterWordFormed(int status){
        switch (status){
            case NameConstant.COREECT_WORD_FORMED:
                strokeColorResourse = R.color.correct_word_color;
                invalidate();
                break;
            case NameConstant.WRONG_WORD_FORMED:
                strokeColorResourse = R.color.wrong_word_color;
                invalidate();
                break;
            case NameConstant.WORD_ALREADY_FORMED:
                strokeColorResourse = R.color.already_formed_word_color;
                invalidate();
                break;
        }
        new android.os.Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                clearCanvas();
            }
        },500);
    }

    private void clearCanvas(){
        strokeColorResourse = R.color.cream_color;
        selectedViews.clear();
        invalidate();
    }

    public void setCurrentLevel(WordConnectLevel currentLevel) {
        this.currentLevel = currentLevel;
    }

    public void setHand(View hand) {
        this.hand = hand;
    }
}
