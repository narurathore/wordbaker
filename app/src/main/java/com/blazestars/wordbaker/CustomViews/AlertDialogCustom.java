package com.blazestars.wordbaker.CustomViews;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blazestars.wordbaker.Activities.MainActivity;
import com.blazestars.wordbaker.Activities.WordConnectGameCanvasActivity;
import com.blazestars.wordbaker.Helper.AnalyticsHelper;
import com.blazestars.wordbaker.Helper.Helper;
import com.blazestars.wordbaker.Helper.MyInterstitialAd;
import com.blazestars.wordbaker.Interfaces.GameScreenListner;
import com.blazestars.wordbaker.Modal.Level;
import com.blazestars.wordbaker.Modal.Stage;
import com.blazestars.wordbaker.Modal.WordConnectLevel;
import com.blazestars.wordbaker.R;
import com.facebook.drawee.view.SimpleDraweeView;

/**
 * Created by narayansingh on 10/05/18.
 */

public class AlertDialogCustom extends AlertDialogBase {
    TextView scoreTV;
    View diamondView;
    public AlertDialogCustom(Context context, AttributeSet attrs) {
        super(context, attrs);
        RelativeLayout view = (RelativeLayout) LayoutInflater.from(context).inflate(R.layout.alert_dailog_custom, null);
        alertDialogContentLayout.addView(view);
        LayoutParams layoutParams = (LayoutParams)view.getLayoutParams();
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        view.setLayoutParams(layoutParams);
        ((SimpleDraweeView)findViewById(R.id.diamond_iv)).setImageURI(Helper.getURIForResourceImage(R.drawable.diamond));
        alertDialogContentLayout.setY(- 1000);
    }

    public void setResult(final Activity activity, final Level level){
        scoreTV = (TextView) findViewById(R.id.score);
        diamondView = findViewById(R.id.diamond_iv);
        final int earnedDiamonds = level.getPoints();
        try {
            String pageviews = "Score Dialog";
            AnalyticsHelper.trackPageView(pageviews, activity);
        } catch (Exception e) {
            e.printStackTrace();
        }
        final TextView noThanksView = (TextView) findViewById(R.id.no_thanks);
        View continueButton = findViewById(R.id.okay);
        TextView title = (TextView) findViewById(R.id.level_completed);
        View scoreLayout = findViewById(R.id.score_layout);
        scoreLayout.setVisibility(VISIBLE);
        TextView subTitle = (TextView) findViewById(R.id.subtitle_view);
        subTitle.setVisibility(GONE);
        title.setText("Level Completed");
        continueButton.setVisibility(VISIBLE);
        scoreTV.setText(" + " + earnedDiamonds);
        scoreTV.setVisibility(VISIBLE);
        if (((level.getLevelNo()) % 3) == 0 && level.getPoints() >= 20){
            noThanksView.setVisibility(VISIBLE);
            noThanksView.setText("No Thanks");
            try {
                String categoryName = "Button";
                String label = "Dialog";
                String action = "No thanks showed";
                AnalyticsHelper.trackEvent(categoryName, action, label, activity);
            } catch (Exception e) {
                e.printStackTrace();
            }
            //continueButton.setBackgroundResource(R.drawable.get3x_button);
            ((SimpleDraweeView)continueButton).setImageURI(Helper.
                    getURIForResourceImage(R.drawable.get3x_button));
        }else {
            noThanksView.setVisibility(GONE);
            //continueButton.setBackgroundResource(R.drawable.continue_button);
            ((SimpleDraweeView)continueButton).setImageURI(Helper.
                    getURIForResourceImage(R.drawable.continue_button));
        }
        if (level.getLevelNo() % 5 == 0){
            new MyInterstitialAd(activity, true);
        }
        continueButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                hideWithAnimation();
                if (noThanksView.getVisibility() == VISIBLE){
                    if (activity instanceof GameScreenListner){
                        ((GameScreenListner) activity).loadVideo();

                    }
                    try {
                        String categoryName = "Button";
                        String label = "Dialog";
                        String action = "Get 3x Diamond clicked";
                        AnalyticsHelper.trackEvent(categoryName, action, label, activity);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                ((GameScreenListner) activity).loadNewStage();
                ((GameScreenListner) activity).animateDiamond(earnedDiamonds);

            }
        });
        noThanksView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                hideWithAnimation();
                if (activity instanceof GameScreenListner){
                    ((GameScreenListner) activity).loadNewStage();
                    ((GameScreenListner) activity).animateDiamond(earnedDiamonds);
                }
                try {
                    String categoryName = "Button";
                    String label = "Dialog";
                    String action = "No thanks clicked";
                    AnalyticsHelper.trackEvent(categoryName, action, label, activity);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void setResult(final Activity activity, final int diamondsEarned){
        scoreTV = (TextView) findViewById(R.id.score);
        View scoreLayout = findViewById(R.id.score_layout);
        scoreLayout.setVisibility(VISIBLE);
        scoreTV.setVisibility(VISIBLE);
        TextView title = (TextView) findViewById(R.id.level_completed);
        TextView subTitle = (TextView) findViewById(R.id.subtitle_view);
        View cancelView = findViewById(R.id.no_thanks);
        cancelView.setVisibility(GONE);
        subTitle.setVisibility(GONE);
        title.setText("Your Reward");
        scoreTV.setText(" + " + diamondsEarned);
        View continueButton = findViewById(R.id.okay);
        //continueButton.setBackgroundResource(R.drawable.okay_button);
        ((SimpleDraweeView)continueButton).setImageURI(Helper.
                getURIForResourceImage(R.drawable.okay_button));
        continueButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                hideWithAnimation();
                if (activity instanceof GameScreenListner){
                    ((GameScreenListner) activity).animateDiamond(diamondsEarned);
                }
            }
        });
        try {
            String pageviews = "Your Reward from video Dialog";
            AnalyticsHelper.trackPageView(pageviews, activity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setResult(final Activity activity, final int diamondsEarned, boolean isDailyReward){
        scoreTV = (TextView) findViewById(R.id.score);
        View scoreLayout = findViewById(R.id.score_layout);
        scoreLayout.setVisibility(VISIBLE);
        scoreTV.setVisibility(VISIBLE);
        TextView title = (TextView) findViewById(R.id.level_completed);
        TextView subTitle = (TextView) findViewById(R.id.subtitle_view);
        View cancelView = findViewById(R.id.no_thanks);
        cancelView.setVisibility(GONE);
        subTitle.setVisibility(GONE);
        title.setText("Your Daily Reward");
        scoreTV.setText(" + " + diamondsEarned);
        View continueButton = findViewById(R.id.okay);
        //continueButton.setBackgroundResource(R.drawable.okay_button);
        ((SimpleDraweeView)continueButton).setImageURI(Helper.
                getURIForResourceImage(R.drawable.okay_button));
        continueButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                hideWithAnimation();
                if (activity instanceof GameScreenListner){
                    ((GameScreenListner) activity).animateDiamond(diamondsEarned);
                }
            }
        });
        try {
            String pageviews = "Your daily reward Dialog";
            AnalyticsHelper.trackPageView(pageviews, activity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setResultToWatchVideoForReward(final Activity activity){
        View scoreLayout = findViewById(R.id.score_layout);
        scoreLayout.setVisibility(VISIBLE);
        scoreTV = (TextView) findViewById(R.id.score);
        TextView subTitle = (TextView) findViewById(R.id.subtitle_view);
        subTitle.setVisibility(GONE);
        scoreTV.setVisibility(VISIBLE);
        TextView cancelView = (TextView) findViewById(R.id.no_thanks);
        cancelView.setVisibility(VISIBLE);
        cancelView.setText("Cancel");
        TextView title = (TextView) findViewById(R.id.level_completed);
        title.setText("Get Free Hint");
        scoreTV.setText(" + 60");
        try {
            String pageviews = "Watch video Dialog";
            AnalyticsHelper.trackPageView(pageviews, activity);
        } catch (Exception e) {
            e.printStackTrace();
        }
        View continueButton = findViewById(R.id.okay);
        //continueButton.setBackgroundResource(R.drawable.watch_button);
        ((SimpleDraweeView)continueButton).setImageURI(Helper.
                getURIForResourceImage(R.drawable.watch_button));
        continueButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                hideWithAnimation();
                if (activity instanceof GameScreenListner){
                    ((GameScreenListner) activity).loadVideo();
                }
                try {
                    String categoryName = "Button";
                    String label = "Dialog";
                    String action = "watch video clicked";
                    AnalyticsHelper.trackEvent(categoryName, action, label, activity);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        cancelView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                hideWithAnimation();
                try {
                    String categoryName = "Button";
                    String label = "Dialog";
                    String action = "Watch video cancel clicked";
                    AnalyticsHelper.trackEvent(categoryName, action, label, activity);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void setResultForSimpleDialog(Activity activity,String title, String message){
        View scoreLayout = findViewById(R.id.score_layout);
        scoreLayout.setVisibility(GONE);
        scoreTV = (TextView) findViewById(R.id.score);
        TextView titleTv = (TextView) findViewById(R.id.level_completed);
        titleTv.setText(title);
        View cancelView = findViewById(R.id.no_thanks);
        cancelView.setVisibility(GONE);
        TextView subTitle = (TextView) findViewById(R.id.subtitle_view);
        if (TextUtils.isEmpty(message)){
            subTitle.setVisibility(GONE);
        }else {
            subTitle.setVisibility(VISIBLE);
            subTitle.setText(message);
        }
        scoreTV.setVisibility(GONE);
        try {
            String categoryName = "Simple Dialog";
            String label = "Message : ";
            if (!TextUtils.isEmpty(message)){
                label += message;
            }

            String action = "Title : " + title;
            if (!TextUtils.isEmpty(title)){
                action += title;
            }
            AnalyticsHelper.trackEvent(categoryName, action, label, activity);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            String pageviews = "Simple Dialog";
            AnalyticsHelper.trackPageView(pageviews, activity);
        } catch (Exception e) {
            e.printStackTrace();
        }
        View continueButton = findViewById(R.id.okay);
        //continueButton.setBackgroundResource(R.drawable.okay_button);
        ((SimpleDraweeView)continueButton).setImageURI(Helper.
                getURIForResourceImage(R.drawable.okay_button));
        continueButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                hideWithAnimation();
            }
        });
    }

    public void setResultRatetingDialog(final Activity activity){
        View scoreLayout = findViewById(R.id.score_layout);
        scoreLayout.setVisibility(GONE);
        scoreTV = (TextView) findViewById(R.id.score);
        TextView titleTv = (TextView) findViewById(R.id.level_completed);
        titleTv.setText("Rate Us");
        TextView cancelView = (TextView) findViewById(R.id.no_thanks);
        cancelView.setVisibility(VISIBLE);
        cancelView.setText("Cancel");
        TextView subTitle = (TextView) findViewById(R.id.subtitle_view);
        try {
            String pageviews = "Rate us Dialog";
            AnalyticsHelper.trackPageView(pageviews, activity);
        } catch (Exception e) {
            e.printStackTrace();
        }
        subTitle.setVisibility(VISIBLE);
        subTitle.setText("If you enjoy using " + activity.getResources().getString(R.string.app_name) + ", please take a moment to rate it. Thanks for you support");
        scoreTV.setVisibility(GONE);
        View continueButton = findViewById(R.id.okay);
        //continueButton.setBackgroundResource(R.drawable.rate_it_button);
        ((SimpleDraweeView)continueButton).setImageURI(Helper.
                getURIForResourceImage(R.drawable.rate_it_button));
        continueButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                hideWithAnimation();
                Uri uri = Uri.parse("market://details?id=" + activity.getPackageName());
                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                        Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET |
                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                try {
                    activity.startActivity(goToMarket);
                } catch (ActivityNotFoundException e) {
                    activity.startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://play.google.com/store/apps/details?id=" + activity.getPackageName())));
                }
                activity.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
                try {
                    String categoryName = "Button";
                    String label = "Dialog";
                    String action = "Rate It clicked";
                    AnalyticsHelper.trackEvent(categoryName, action, label, activity);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        cancelView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                hideWithAnimation();
                SharedPreferences.Editor editor = activity.getSharedPreferences("isRatingDialogShowed", Context.MODE_PRIVATE).edit();
                editor.putBoolean("isRatingDialogShowed", false);
                editor.commit();
                activity.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
                try {
                    String categoryName = "Button";
                    String label = "Dialog";
                    String action = "Rate It cancel clicked";
                    AnalyticsHelper.trackEvent(categoryName, action, label, activity);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void setResult(final Activity activity, final WordConnectLevel level, final boolean isDailyPuzzle,
                          int adAfterStageCount){
        scoreTV = (TextView) findViewById(R.id.score);
        diamondView = findViewById(R.id.diamond_iv);
        final int earnedDiamonds = level.getPoints();
        try {
            String pageviews = "Score Dialog";
            AnalyticsHelper.trackPageView(pageviews, activity);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            String categoryName = "Level";
            String label = "" + level.getLevelNo();
            String action = "Completed";
            AnalyticsHelper.trackEvent(categoryName, action, label, activity);
        } catch (Exception e) {
            e.printStackTrace();
        }
        final TextView noThanksView = (TextView) findViewById(R.id.no_thanks);
        View continueButton = findViewById(R.id.okay);
        TextView title = (TextView) findViewById(R.id.level_completed);
        View scoreLayout = findViewById(R.id.score_layout);
        scoreLayout.setVisibility(VISIBLE);
        TextView subTitle = (TextView) findViewById(R.id.subtitle_view);
        subTitle.setVisibility(GONE);
        if (isDailyPuzzle){
            title.setText("Daily Puzzle Completed");
        }else {
            title.setText("Level Completed");
        }
        continueButton.setVisibility(VISIBLE);
        scoreTV.setText(" + " + earnedDiamonds);
        scoreTV.setVisibility(VISIBLE);
        if (((level.getLevelNo()) % 3 == 0 && level.getPoints() >= 20) || isDailyPuzzle){
            noThanksView.setVisibility(VISIBLE);
            noThanksView.setText("No Thanks");
            //continueButton.setBackgroundResource(R.drawable.get3x_button);
            ((SimpleDraweeView)continueButton).setImageURI(Helper.
                    getURIForResourceImage(R.drawable.get3x_button));
            try {
                String categoryName = "Button";
                String label = "Dialog";
                String action = "No thanks showed";
                AnalyticsHelper.trackEvent(categoryName, action, label, activity);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                String categoryName = "Button";
                String label = "Dialog";
                String action = "Get 3x Diamond showed";
                AnalyticsHelper.trackEvent(categoryName, action, label, activity);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {
            noThanksView.setVisibility(GONE);
            //continueButton.setBackgroundResource(R.drawable.continue_button);
            ((SimpleDraweeView)continueButton).setImageURI(Helper.
                    getURIForResourceImage(R.drawable.continue_button));
        }
        if (isDailyPuzzle || (level.getLevelNo() % adAfterStageCount == 0)) {
            /*if (((WordConnectGameCanvasActivity)activity).myInterstitialAd != null) {
                ((WordConnectGameCanvasActivity)activity).myInterstitialAd.loadAndShowInterstitialAc();
            }*/
            new MyInterstitialAd(activity, true);
        }
        continueButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                hideWithAnimation();
                if (noThanksView.getVisibility() == VISIBLE){
                    if (activity instanceof GameScreenListner){
                        ((GameScreenListner) activity).loadVideo();
                    }
                    try {
                        String categoryName = "Button";
                        String label = "Dialog";
                        String action = "Get 3x Diamond clicked";
                        AnalyticsHelper.trackEvent(categoryName, action, label, activity);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                ((GameScreenListner) activity).loadNewStage();
                ((GameScreenListner) activity).animateDiamond(earnedDiamonds);

            }
        });
        noThanksView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                hideWithAnimation();
                try {
                    String categoryName = "Button";
                    String label = "Dialog";
                    String action = "No thanks clicked";
                    AnalyticsHelper.trackEvent(categoryName, action, label, activity);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (activity instanceof GameScreenListner){
                    ((GameScreenListner) activity).loadNewStage();
                    ((GameScreenListner) activity).animateDiamond(earnedDiamonds);
                }

            }
        });
    }
}
