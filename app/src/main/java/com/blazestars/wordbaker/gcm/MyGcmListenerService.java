
/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blazestars.wordbaker.gcm;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.util.Log;


import com.blazestars.wordbaker.Activities.MainActivity;
import com.blazestars.wordbaker.Activities.StageActivity;
import com.blazestars.wordbaker.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MyGcmListenerService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    private String notification_type;
    private int _id;
    private String _title;
    private String _message;

    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        try {
            // TODO(developer): Handle FCM messages here.
            // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
            Log.d(TAG, "From: " + remoteMessage.getFrom());

            // Check if message contains a data payload.
            if (remoteMessage.getData().size() > 0) {
                Log.d(TAG, "Message data payload: " + remoteMessage.getData());
                _title = remoteMessage.getData().get("title");
                _message=remoteMessage.getData().get("alert");
            }
            //if (Helper.isPushNotificationEnabled(this)) {
                handleNofificationNavigation(this, remoteMessage);
            //}
        } catch (Exception e) {
            Log.d(TAG, "gcm error: " + e.getMessage());
            e.printStackTrace();
        }

        // [START_EXCLUDE]
        /**
         * Production applications would usually process the message here.
         * Eg: - Syncing with server.
         *     - Store message in local database.
         *     - Update UI.
         */

        /**
         * In some cases it may be useful to show a notification indicating to the user
         * that a message was received.
         */

        // [END_EXCLUDE]
    }
    // [END receive_message]



    private void handleNofificationNavigation(Context context, RemoteMessage remoteMessage) {
        try {

            notification_type = remoteMessage.getData().get("notification_type");
            if(!"".equalsIgnoreCase(notification_type)) {
                if (notification_type.equalsIgnoreCase("OPEN_APP")) {
                    openApp(context);
                }else if (notification_type.equalsIgnoreCase("OPEN_APP_WITH_HINT")) {
                    String diamondCount = "20";
                    if (remoteMessage.getData().containsKey("diamond_count")){
                        diamondCount = remoteMessage.getData().get("diamond_count");
                    }
                    openAppWithNewHint(context,diamondCount);
                }else if (notification_type.equalsIgnoreCase("OPEN_APP_FOR_DAILY_PUZZLE")) {
                    openAppDailyPuzzle(context);
                }else {
                    openApp(context);
                }
            }else {
                openApp(context);
            }
        } catch (Exception e) {

        }
    }

    private void push(Context _context, PendingIntent contentIntent) {
        int notifyID = _id;
        Bitmap notificationLargeIconBitmap = BitmapFactory.decodeResource(
                _context.getResources(),
                R.drawable.notification_icon);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(_context)
                        .setContentTitle(_title)
                        .setTicker(null)
                        .setContentText(_message)
                        .setSmallIcon(R.drawable.notification_icon)
                        .setLargeIcon(notificationLargeIconBitmap)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(_title));
        mBuilder.setTicker(null);
        mBuilder.setPriority(Notification.PRIORITY_HIGH);
        mBuilder.setAutoCancel(true);
        mBuilder.setContentIntent(contentIntent);
        mBuilder.setDefaults(Notification.DEFAULT_ALL);
        mBuilder.setDefaults(Notification.DEFAULT_VIBRATE);
        NotificationManager mNotificationManager = (NotificationManager)
                _context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(0, mBuilder.build());
        AlarmManager alarmManager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(contentIntent);
    }

    private void openApp(Context context) {
        Intent intent;
        intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent contentIntent = PendingIntent.getActivity(context, _id,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);

        push(context, contentIntent);
    }

    private void openAppWithNewHint(Context context, String diamondCount) {
        Intent intent;
        intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("isHint",true);
        intent.putExtra("diamond_count", diamondCount);
        PendingIntent contentIntent = PendingIntent.getActivity(context, _id,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);
        push(context, contentIntent);
    }

    private void openAppDailyPuzzle(Context context) {
        Intent intent;
        intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("isForDailyPuzzle",true);
        PendingIntent contentIntent = PendingIntent.getActivity(context, _id,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);
        push(context, contentIntent);
    }
}