package com.blazestars.wordbaker.Helper;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PixelFormat;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.view.View;
import android.widget.Switch;

import com.blazestars.wordbaker.BuildConfig;
import com.blazestars.wordbaker.CustomViews.AlertDialogCustom;
import com.blazestars.wordbaker.Modal.NameConstant;
import com.blazestars.wordbaker.R;
import com.facebook.common.util.UriUtil;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class Helper {

	public static int convertDipToPixels(float dips, Context context)
	{
		return (int) (dips * context.getResources().getDisplayMetrics().density + 0.5f);
	}

	public static void setSoundOnOff(Context context, Boolean pushBool)
	{
		SharedPreferences.Editor editor = context.getSharedPreferences("soundEnabled", Context.MODE_PRIVATE).edit();
		editor.putBoolean("soundEnabled", pushBool);
		editor.commit();

	}

	public static Boolean isSoundEnabled(Context context)
	{
		SharedPreferences prefs = context.getSharedPreferences("soundEnabled", Context.MODE_PRIVATE);
		return prefs.getBoolean("soundEnabled",true);

	}

	public static String getCachedDataForUrl(String url, Context context){
		SharedPreferences sharedPreferences = context.getSharedPreferences(NameConstant.HTTP_CLIENT_CACHE_DATABASE, context.MODE_PRIVATE);
		String data = sharedPreferences.getString(String.valueOf(url.hashCode()), "");
		return data;
	}

	public static void saveCacheDataForUrl(String url, String string, Context context){
		SharedPreferences.Editor sharedPrefencesEditor = context.getSharedPreferences(NameConstant.HTTP_CLIENT_CACHE_DATABASE,context.MODE_PRIVATE).edit();
		sharedPrefencesEditor.putString(String.valueOf(url.hashCode()), string);
		sharedPrefencesEditor.apply();
	}



	public static boolean isNetworkAvailable(Context mcontext) {
		ConnectivityManager connectivityManager
				= (ConnectivityManager) mcontext.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null;
	}


	public static void showRatingDialog(final Activity activity, AlertDialogCustom alertDialogCustom, int level){
		if (level % 15 == 0) {
			SharedPreferences prefs = activity.getSharedPreferences("isRatingDialogShowed", Context.MODE_PRIVATE);
			if (!prefs.getBoolean("isRatingDialogShowed", false)) {
				SharedPreferences.Editor editor = activity.getSharedPreferences("isRatingDialogShowed", Context.MODE_PRIVATE).edit();
				editor.putBoolean("isRatingDialogShowed", true);
				editor.commit();
				alertDialogCustom.setResultRatetingDialog(activity);
				alertDialogCustom.showWithAnimation();
			}
		}
	}

	public static int getLetterImageResID(char letter){
		return R.drawable.plus;
	}

	public static void playGameSound(Context context, int soundResourceFile){
		if (Helper.isSoundEnabled(context)) {
			final MediaPlayer mp = MediaPlayer.create(context, soundResourceFile);
			if (mp != null) {
				mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
					@Override
					public void onCompletion(MediaPlayer mp) {
						if (mp != null) {
							mp.release();
						}
					}
				});
				mp.start();
			}
		}
	}

	public static String getCurrentDate(){
		Date c = Calendar.getInstance().getTime();

		SimpleDateFormat df = new SimpleDateFormat("dd-MM");
		return df.format(c);
	}

	public static int getResourceOfBackGround(){
		Random r = new Random();
		int number = r.nextInt(18 - 1) + 1;
		switch (number){
			case 1:
				return R.drawable.game_background_1;
			case 2:
				return R.drawable.game_background_2;
			case 3:
				return R.drawable.game_background_3;
			case 4:
				return R.drawable.game_background_4;
			case 5:
				return R.drawable.game_background_5;
			case 6:
				return R.drawable.game_background_25;
			case 7:
				return R.drawable.game_background_7;
			case 8:
				return R.drawable.game_background_8;
			case 9:
				return R.drawable.game_background_9;
			case 10:
				return R.drawable.game_background_10;
			case 11:
				return R.drawable.game_background_24;
			case 12:
				return R.drawable.game_background_23;
			case 13:
				return R.drawable.game_background_13;
			case 14:
				return R.drawable.game_background_18;
			case 15:
				return R.drawable.game_background_19;
			case 16:
				return R.drawable.game_background_16;
			case 17:
				return R.drawable.game_background_18;

		}
		return R.drawable.game_screen_background;
	}

	public static Bitmap getScreenShot(View view) {
		//View screenView = view.getRootView();
		view.setDrawingCacheEnabled(true);
		Bitmap bitmap = Bitmap.createBitmap(view.getDrawingCache());
		view.setDrawingCacheEnabled(false);
		return bitmap;
	}

	public static void shareLevelImage(Context context,View view){
		try {
			Bitmap bitmap = getScreenShot(view);
			try {
				File cachePath = new File(context.getCacheDir(), "images");
				cachePath.mkdirs(); // don't forget to make the directory
				FileOutputStream stream = new FileOutputStream(cachePath + "/image.png"); // overwrites this image every time
				bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
				stream.close();

			} catch (Exception e) {
				e.printStackTrace();
			}
			File imagePath = new File(context.getCacheDir(), "images");
			File newFile = new File(imagePath, "image.png");
			Uri contentUri = FileProvider.getUriForFile(context, "com.blazestars.wordbaker", newFile);

			if (contentUri != null) {
				Intent shareIntent = new Intent();
				String appLink = "\n http://play.google.com/store/apps/details?id=" + context.getPackageName();
				shareIntent.setAction(Intent.ACTION_SEND);
				shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION); // temp permission for receiving app to read this file
				shareIntent.setDataAndType(contentUri, context.getContentResolver().getType(contentUri));
				shareIntent.putExtra(Intent.EXTRA_STREAM, contentUri);
				shareIntent.putExtra(Intent.EXTRA_TEXT, "Help me to solve this puzzle\nDownload Word Baker game from here :" + appLink);
				shareIntent.setType("image/png");
				context.startActivity(Intent.createChooser(shareIntent, "Share with :"));
			}
		}catch (Exception ex){
			ex.printStackTrace();
		}

	}

	public static Uri getURIForResourceImage(int res){
		return new Uri.Builder()
				.scheme(UriUtil.LOCAL_RESOURCE_SCHEME)
				.path(String.valueOf(res))
				.build();
	}
}