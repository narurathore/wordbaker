package com.blazestars.wordbaker.Helper;
/**
 * 
 *@file MyInterstitialAd.java
Handles and generates interstitial ad
 *@author Readwhere
 *
 */

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.blazestars.wordbaker.Activities.MainActivity;
import com.blazestars.wordbaker.Modal.GameData;
import com.blazestars.wordbaker.Modal.NameConstant;
import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.InterstitialAdListener;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

/**
Handles and generates interstitial ad
 *@author Readwhere
 *
 *
 */
public class MyInterstitialAd {
	public InterstitialAd interstitial;
	private static final String TAG = "MyInterstitialAd";
	private AdRequest adRequest;
	private com.facebook.ads.InterstitialAd interstitialFacebookAd;


	public MyInterstitialAd(Context context, boolean showAdWhenLoaded) {
		GameData gameData = new APICallHelper().getGameDataCached(context);
		if (gameData.getInterstitialAdPriority() == NameConstant.FACEBOOK_AD){
			initializeFacebookAd(context);
		}else {
			initializeAd(context, showAdWhenLoaded);
		}
	}

	private void initializeFacebookAd(final Context context){
		interstitialFacebookAd = new com.facebook.ads.InterstitialAd(context, "219755418737678_219761562070397");
		// Set listeners for the Interstitial Ad
		interstitialFacebookAd.setAdListener(new InterstitialAdListener() {
			@Override
			public void onInterstitialDisplayed(Ad ad) {
				// Interstitial ad displayed callback
				Log.e(TAG, "Interstitial ad displayed.");
			}

			@Override
			public void onInterstitialDismissed(Ad ad) {
				// Interstitial dismissed callback
				Log.e(TAG, "Interstitial ad dismissed.");
			}

			@Override
			public void onError(Ad ad, AdError adError) {
				// Ad error callback
				Log.e(TAG, "Interstitial ad failed to load: " + adError.getErrorMessage());
				initializeAd(context, true);
			}

			@Override
			public void onAdLoaded(Ad ad) {
				// Interstitial ad is loaded and ready to be displayed
				Log.d(TAG, "Interstitial ad is loaded and ready to be displayed!");
				// Show the ad
				interstitialFacebookAd.show();
			}

			@Override
			public void onAdClicked(Ad ad) {
				// Ad clicked callback
				Log.d(TAG, "Interstitial ad clicked!");
			}

			@Override
			public void onLoggingImpression(Ad ad) {
				// Ad impression logged callback
				Log.d(TAG, "Interstitial ad impression logged!");
			}
		});

		// For auto play video ads, it's recommended to load the ad
		// at least 30 seconds before it is shown
		interstitialFacebookAd.loadAd();

	}
	/**
	Generates Interstitial Ad
	 *@author Readwhere
	 *@param context , parent context
	 *@return void
	 */
	private void initializeAd(final Context context, final boolean showAdWhenLoaded){

		
		interstitial = new InterstitialAd((Activity) context);

			adRequest = new AdRequest.Builder().build();
		interstitial.setAdUnitId("ca-app-pub-2649338194133294/8770163540");
				interstitial.loadAd(adRequest);

		
		interstitial.setAdListener(new AdListener() {
			@Override
			  public void onAdLoaded() {
					if (showAdWhenLoaded && interstitial.isLoaded()){
						interstitial.show();
					}
			  }

			  @Override
			  public void onAdFailedToLoad(int errorCode) {
				  String message = "onFailedToReceiveAd (" + errorCode + ")";

			  }

			  @Override
			  public void onAdOpened() {

			    // Save app state before going to the ad overlay.
			  }

			  @Override
			  public void onAdClosed() {

			    // Save app state before going to the ad overlay.
			  }

			  @Override
			  public void onAdLeftApplication() {

			    // Save app state before going to the ad overlay.
			  }
		});
	}

	public void showAd(){
		if (interstitial != null && interstitial.isLoaded()){
			interstitial.show();
		}
	}
}
