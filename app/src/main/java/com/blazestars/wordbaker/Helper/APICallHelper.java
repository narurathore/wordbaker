package com.blazestars.wordbaker.Helper;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

import com.android.volley.VolleyError;
import com.blazestars.wordbaker.Activities.MainActivity;
import com.blazestars.wordbaker.Modal.GameData;
import com.blazestars.wordbaker.Modal.NameConstant;
import com.blazestars.wordbaker.Network.VolleyData;
import com.google.gson.Gson;

import org.json.JSONObject;


public class APICallHelper {

    public GameData getGameDataCached(final Context context) {
        String url = NameConstant.GET_GAME_DATA_URL;
        String response = Helper.getCachedDataForUrl(url, context);
        GameData gameData = new Gson().fromJson(response,GameData.class);
        if (gameData != null){
            if (gameData.getStages() != null && gameData.getStages().size() > 0){
                gameData.getStages().get(0).openStage(context);
            }
            gameData.setLockUnlockedStateFromSharedPref(context);
        }
        return gameData;
    }
}
