package com.blazestars.wordbaker.Modal;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by narayansingh on 24/05/18.
 */

public class WordConnectLevel {
    @SerializedName("words")
    private List<String> words;
    @SerializedName("forDate")
    private String forDate;
    @SerializedName("id")
    private int id;
    @SerializedName("levelNo")
    private int levelNo = -1;
    @SerializedName("points")
    private int points = 20;
    @SerializedName("wordPoint")
    private int wordPoint = 5;
    private List<String> wordsFormed = new ArrayList<>();
    @SerializedName("extraWords")
    private List<String> extraWords = new ArrayList<>();
    private List<String> extraWordsFormed = new ArrayList<>();
    @SerializedName("puzzleLetters")
    private List<WordConnectLevelLetter> puzzleLetters;
    @SerializedName("canvasLetters")
    private String canvasLetters;
    private boolean isCompleted;

    private int xMax = -1;
    private int yMax = -1;

    public List<String> getWords() {
        return words;
    }

    public void setWords(List<String> words) {
        this.words = words;
    }

    public boolean isCompleted() {
        return isCompleted;
    }

    public String getCanvasLetters() {
        return canvasLetters;
    }

    public List<WordConnectLevelLetter> getPuzzleLetters() {
        return puzzleLetters;
    }

    public List<String> getWordsFormed() {
        return wordsFormed;
    }

    public void addWordsFormed(Context context,String word, boolean isDailyPuzzle) {
        this.wordsFormed.add(word.toLowerCase());
        try {
            String wordsFormed = "";
            for (int i = 0; i < this.wordsFormed.size(); i++){
                wordsFormed += this.wordsFormed.get(i);
                if (i != this.wordsFormed.size() - 1){
                    wordsFormed += ",";
                }
            }
            SharedPreferences.Editor editor = context.getSharedPreferences(NameConstant.STAGE_LEVEL_COMPLETED_DATABASE, MODE_PRIVATE).edit();
            if (isDailyPuzzle) {
                editor.putString("level_word_connect_words_daily_puzzle" + id, wordsFormed);
            }else {
                editor.putString("level_word_connect_words" + id, wordsFormed);
            }
            editor.apply();
        }catch (Exception ex){

        }

    }

    public boolean isWordAlreadyFormed(String word){
        return wordsFormed.contains(word.toLowerCase());
    }

    public boolean isWordFormed(String word){
            for (String extraWord : words){
                if (extraWord.equalsIgnoreCase(word)){
                    return true;
                }
            }
            return false;

    }

    public List<WordConnectLevelLetter> getLettersOfWordsFormed(String word){
        List<WordConnectLevelLetter> letterOfWordFormed = new ArrayList<>();
        if (isWordFormed(word)){
            for (WordConnectLevelLetter letter: getPuzzleLetters()){
                letterOfWordFormed = getLettersOfWordsFormedWithStartingLetter(letter, word);
                if (letterOfWordFormed.size() == word.length()){
                    break;
                }else{
                    letterOfWordFormed.clear();
                }
            }
        }
        return letterOfWordFormed;
    }

    private List<WordConnectLevelLetter> getLettersOfWordsFormedWithStartingLetter(WordConnectLevelLetter initialLetter, String word){
        List<WordConnectLevelLetter> letterOfWordsFormed = new ArrayList<>();
        WordConnectLevelLetter tempLetter = new WordConnectLevelLetter();
        tempLetter.setY(initialLetter.getY());
        tempLetter.setX(initialLetter.getX());
        tempLetter.setLetter(String.valueOf(initialLetter.getLetter()));
        int tempIndex = -1;
        //check horizontal
        int i = 0;
        for (i = 0; i < word.length(); i++){
            if (word.toLowerCase().charAt(i) != tempLetter.getLetter()){
                break;
            }
            letterOfWordsFormed.add(getPuzzleLetters().get(getPuzzleLetters().indexOf(tempLetter)));
            int x = tempLetter.getX() + 1;
            tempLetter = new WordConnectLevelLetter();
            tempLetter.setY(initialLetter.getY());
            tempLetter.setX(x);
            if (i + 1 < word.length()) {
                tempLetter.setLetter(String.valueOf(word.charAt(i + 1)));
            }else {
                break;
            }
            if (!getPuzzleLetters().contains(tempLetter)){
                break;
            }

        }
        if(letterOfWordsFormed.size() == word.length()){
            if (!isLetterPresentAt(letterOfWordsFormed.get(0).getX() - 1,
                    letterOfWordsFormed.get(0).getY())
                    && !isLetterPresentAt(letterOfWordsFormed.get(letterOfWordsFormed.size() - 1).getX() + 1,
                    letterOfWordsFormed.get(letterOfWordsFormed.size() - 1).getY())) {
                return letterOfWordsFormed;
            }
        }
        letterOfWordsFormed.clear();
        tempLetter.setY(initialLetter.getY());
        tempLetter.setX(initialLetter.getX());
        tempLetter.setLetter(String.valueOf(initialLetter.getLetter()));
        //check vertical
        for (i = 0; i < word.length(); i++){
            if (word.toLowerCase().charAt(i) != tempLetter.getLetter()){
                break;
            }
            letterOfWordsFormed.add(getPuzzleLetters().get(getPuzzleLetters().indexOf(tempLetter)));
            int y = tempLetter.getY() + 1;
            tempLetter = new WordConnectLevelLetter();
            tempLetter.setX(initialLetter.getX());
            tempLetter.setY(y);
            if (i + 1 < word.length()) {
                tempLetter.setLetter(String.valueOf(word.charAt(i + 1)));
            }else {
                break;
            }
            if (!getPuzzleLetters().contains(tempLetter)){
                break;
            }
        }
        if(letterOfWordsFormed.size() == word.length()){
            if (!isLetterPresentAt(letterOfWordsFormed.get(0).getX(),
                    letterOfWordsFormed.get(0).getY() - 1)
                    && !isLetterPresentAt(letterOfWordsFormed.get(letterOfWordsFormed.size() - 1).getX(),
                    letterOfWordsFormed.get(letterOfWordsFormed.size() - 1).getY() + 1)) {
                return letterOfWordsFormed;
            }
        }
        return new ArrayList<>();
    }

    public int getLevelNo() {
        return levelNo;
    }

    public void setLevelNo(int levelNo) {
        this.levelNo = levelNo;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public boolean isExtraWordFormed(String word){
        for (String extraWord : extraWords){
            if (extraWord.equalsIgnoreCase(word)){
                return true;
            }
        }
        return false;
    }

    public void addExtraWordFormed(String word){
        extraWordsFormed.add(word.toLowerCase());
    }

    public boolean isExtraWordAlreadyFormed(String word){
        return extraWordsFormed.contains(word.toLowerCase());
    }

    public int getWordPoint() {
        return wordPoint;
    }

    public void setWordPoint(int wordPoint) {
        this.wordPoint = wordPoint;
    }

    public WordConnectLevelLetter getLetterForNextHint(){
        int minIndex = 9;
        WordConnectLevelLetter letterWithMinIndex = null;
        List<WordConnectLevelLetter> wordLetters;
            for (String word : words){
                if (!isWordAlreadyFormed(word)){
                    wordLetters = getLettersOfWordsFormed(word);
                    for (int i = 0; i < wordLetters.size(); i++){
                        if (!wordLetters.get(i).isLetterFilled()){
                            if (minIndex > i){
                                minIndex = i;
                                letterWithMinIndex = wordLetters.get(i);
                            }
                            break;
                        }
                    }
                }
            }

        return letterWithMinIndex;
    }

    public void setCompleted(boolean levelCompleted, Context context, boolean isDailyPuzzle) {
        isCompleted = levelCompleted;
        SharedPreferences.Editor editor = context.getSharedPreferences(NameConstant.STAGE_LEVEL_COMPLETED_DATABASE, MODE_PRIVATE).edit();
        if (isDailyPuzzle){
            editor.putBoolean("level_word_connect_daily_puzzle" + id, levelCompleted);
        }else {
            editor.putBoolean("level_word_connect" + id, levelCompleted);
        }
        editor.apply();
    }

    public void updateCompletedStateFromSharedPref(Context context, boolean isDailyPuzzle) {
        SharedPreferences pref = context.getSharedPreferences(NameConstant.STAGE_LEVEL_COMPLETED_DATABASE, MODE_PRIVATE);
        if (isDailyPuzzle){
            isCompleted = pref.getBoolean("level_word_connect_daily_puzzle" + id, false);
        }else {
            isCompleted = pref.getBoolean("level_word_connect" + id, false);
        }
        try {
            if (!isCompleted) {
                String[] wordsFormed;
                if (isDailyPuzzle){
                    wordsFormed = pref.getString("level_word_connect_words_daily_puzzle" + id, "").split(",");
                }else {
                    wordsFormed = pref.getString("level_word_connect_words" + id, "").split(",");
                }
                for (int i = 0; i < wordsFormed.length; i++) {
                    this.wordsFormed.add(wordsFormed[i]);
                }
            }

        }catch (Exception ex){

        }
    }

    public int getXOrYWidthMax() {
        if (xMax == -1 || yMax == -1){
            for (WordConnectLevelLetter wordConnectLevelLetter : puzzleLetters){
                if (xMax < wordConnectLevelLetter.getX()){
                    xMax = wordConnectLevelLetter.getX();
                }
                if (yMax < wordConnectLevelLetter.getY()){
                    yMax = wordConnectLevelLetter.getY();
                }
            }
        }
        int tempXMax = xMax;
        if (xMax < 3) {
            tempXMax++;
        }
        int tempYMax = yMax;
        if (yMax < 3) {
            tempYMax++;
        }
        return (tempXMax > tempYMax ? tempXMax : tempYMax) + 1;
    }

    public int getXMax() {
        if (xMax == -1){
            for (WordConnectLevelLetter wordConnectLevelLetter : puzzleLetters){
                if (xMax < wordConnectLevelLetter.getX()){
                    xMax = wordConnectLevelLetter.getX();
                }
            }
        }
        return xMax + 1;
    }

    public int getYMax() {
        if (yMax == -1){
            for (WordConnectLevelLetter wordConnectLevelLetter : puzzleLetters){
                if (yMax < wordConnectLevelLetter.getY()){
                    yMax = wordConnectLevelLetter.getY();
                }
            }
        }
        return yMax + 1;
    }

    public void clearLevelToPlayAgain(Context context, boolean isDailyPuzzle){
        setCompleted(false, context, isDailyPuzzle);
        //wordsFormed.clear();
        //extraWordsFormed.clear();
    }

    public boolean isLetterPresentAt(int x, int y){
        for (WordConnectLevelLetter wordConnectLevelLetter : getPuzzleLetters()){
            if (wordConnectLevelLetter.getX() == x && wordConnectLevelLetter.getY() == y){
                return true;
            }
        }
        return false;
    }

    public boolean isAllCharacterFilled(){
        for (WordConnectLevelLetter wordConnectLevelLetter : puzzleLetters){
            if (!wordConnectLevelLetter.isLetterFilled())  {
                return false;
            }
        }
        return true;
    }

    public String getForDate() {
        return forDate;
    }

    public void setForDate(String forDate) {
        this.forDate = forDate;
    }

    public String getNextUnformedWord(){
        boolean wordFound = false;
        for (String word : words){
            for (String wordFormed : wordsFormed){
                if (word.equalsIgnoreCase(wordFormed)){
                    wordFound = true;
                    break;
                }
            }
            if (!wordFound){
                return word;
            }
            wordFound = false;
        }
        return null;
    }
}
