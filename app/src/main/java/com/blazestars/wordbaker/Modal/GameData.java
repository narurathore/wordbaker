
package com.blazestars.wordbaker.Modal;

import android.content.Context;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

import com.blazestars.wordbaker.Helper.Helper;
import com.google.gson.annotations.SerializedName;

public class GameData {

    @SerializedName("ad_after_stage_count")
    private int adAfterStageCount = 4;

    @SerializedName("spin_after_min")
    private int spinAfterMin = 5;

    @SerializedName("refersh_facebook_bannner_ad_after_secs")
    private int refershFacebookBannnerAdAfterSecs = 45;

    @SerializedName("interstitial_ad_priority")
    private int interstitialAdPriority = 1;

    @SerializedName("banner_ad_priority")
    private int bannerAdPriority = 1;

    @SerializedName("video_ad_priority")
    private int videoAdPriority = 1;

    @SerializedName("stages")
    private List<Stage> stages = null;

    public List<Stage> getStages() {
        return stages;
    }

    public void setStages(List<Stage> stages) {
        this.stages = stages;
    }

    @SerializedName("word_connect_levels")
    private List<WordConnectLevel> wordConnectLevels;

    @SerializedName("word_connect_levels_daily_puzzle")
    private List<WordConnectLevel> wordConnectLevelsDailyPuzzle;

    public void setLockUnlockedStateFromSharedPref(Context context){


        if (stages != null){
            for (Stage stage : stages){
                stage.updateLockedStateFromSharedPref(context);
                stage.updateCompletedStateFromSharedPref(context);
            }
        }
        if (wordConnectLevels != null){
            for (WordConnectLevel wordConnectLevel : wordConnectLevels){
                wordConnectLevel.updateCompletedStateFromSharedPref(context,false);
                if (wordConnectLevel.getLevelNo() <= 189){
                    //wordConnectLevel.setCompleted(true,context, false);
                }
            }
        }

        if (wordConnectLevelsDailyPuzzle != null){
            for (WordConnectLevel wordConnectLevel : wordConnectLevelsDailyPuzzle){
                wordConnectLevel.updateCompletedStateFromSharedPref(context,true);
            }
        }
    }

    public Stage getLastStageUnlocked(){
        for (Stage stage : stages){
            if (!stage.isCompleted()){
                return stage;
            }
        }
        return null;
    }

    public List<WordConnectLevel> getWordConnectLevels() {
        return wordConnectLevels;
    }

    public WordConnectLevel getLastUnfinishedWordConnectLevel(){
        if (wordConnectLevels != null) {
            for (WordConnectLevel wordConnectLevel : wordConnectLevels) {
                if (!wordConnectLevel.isCompleted()) {
                    return wordConnectLevel;
                }
            }
        }
        return null;
    }

    public WordConnectLevel getDailyPuzzle(){
        if (wordConnectLevelsDailyPuzzle != null && wordConnectLevelsDailyPuzzle.size() > 0){
            String dateString = Helper.getCurrentDate();
            //dateString = "28-06";
            if (TextUtils.isEmpty(dateString)){
                return wordConnectLevelsDailyPuzzle.get(0);
            }else {
                for (WordConnectLevel wordConnectLevel : wordConnectLevelsDailyPuzzle) {
                    if (dateString.equalsIgnoreCase(wordConnectLevel.getForDate())){
                        return wordConnectLevel;
                    }
                }
            }
        }
        return null;
    }

    public int getAdAfterStageCount() {
        return adAfterStageCount;
    }

    public void setAdAfterStageCount(int adAfterStageCount) {
        this.adAfterStageCount = adAfterStageCount;
    }

    public int getSpinAfterMin() {
        return spinAfterMin;
    }

    public void setSpinAfterMin(int spinAfterMin) {
        this.spinAfterMin = spinAfterMin;
    }

    public int getInterstitialAdPriority() {
        return interstitialAdPriority;
    }

    public void setInterstitialAdPriority(int interstitialAdPriority) {
        this.interstitialAdPriority = interstitialAdPriority;
    }

    public int getBannerAdPriority() {
        return bannerAdPriority;
    }

    public void setBannerAdPriority(int bannerAdPriority) {
        this.bannerAdPriority = bannerAdPriority;
    }

    public int getVideoAdPriority() {
        return videoAdPriority;
    }

    public void setVideoAdPriority(int videoAdPriority) {
        this.videoAdPriority = videoAdPriority;
    }

    public int getRefershFacebookBannnerAdAfterSecs() {
        return refershFacebookBannnerAdAfterSecs;
    }

    public void setRefershFacebookBannnerAdAfterSecs(int refershFacebookBannnerAdAfterSecs) {
        this.refershFacebookBannnerAdAfterSecs = refershFacebookBannnerAdAfterSecs;
    }
}
