package com.blazestars.wordbaker.Modal;

import com.google.gson.annotations.SerializedName;

/**
 * Created by narayansingh on 24/05/18.
 */

public class WordConnectLevelLetter {
    @SerializedName("i")
    private int x;
    @SerializedName("j")
    private int y;
    @SerializedName("letter")
    private String letter;
    private boolean isLetterFilled;

    public char getLetter() {
        return letter.toLowerCase().charAt(0);
    }

    public void setLetter(String letter) {
        this.letter = letter;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof WordConnectLevelLetter){
            if (getLetter() == ((WordConnectLevelLetter) obj).getLetter()
                    && getX() == ((WordConnectLevelLetter) obj).getX()
                    && getY() == ((WordConnectLevelLetter) obj).getY()){
                return true;
            }
        }
        return false;
    }

    public boolean isLetterFilled() {
        return isLetterFilled;
    }

    public void setLetterFilled(boolean letterFilled) {
        isLetterFilled = letterFilled;
    }
}
