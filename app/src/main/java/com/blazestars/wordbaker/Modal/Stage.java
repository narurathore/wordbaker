
package com.blazestars.wordbaker.Modal;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import static android.content.Context.MODE_PRIVATE;

public class Stage implements Parcelable{

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("stageNo")
    @Expose
    private int stageNo;
    @SerializedName("points")
    @Expose
    private int points;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("levels")
    @Expose
    private ArrayList<Level> levels = null;

    @SerializedName("isLocked")
    @Expose
    private boolean isLocked = true;

    @SerializedName("isCompleted")
    @Expose
    private boolean isCompleted = false;

    protected Stage(Parcel in) {
        id = in.readInt();
        name = in.readString();
        stageNo = in.readInt();
        points = in.readInt();
        image = in.readString();
        levels = in.createTypedArrayList(Level.CREATOR);
        isLocked = in.readByte() != 0;
        isCompleted = in.readByte() != 0;
    }

    public static final Creator<Stage> CREATOR = new Creator<Stage>() {
        @Override
        public Stage createFromParcel(Parcel in) {
            return new Stage(in);
        }

        @Override
        public Stage[] newArray(int size) {
            return new Stage[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStageNo() {
        return stageNo;
    }

    public void setStageNo(int stageNo) {
        this.stageNo = stageNo;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public ArrayList<Level> getLevels() {
        return levels;
    }

    public void setLevels(ArrayList<Level> levels) {
        this.levels = levels;
    }

    public boolean isLocked() {
        return isLocked;
    }

    public void openStage(Context context){
        isLocked = false;
        SharedPreferences.Editor editor = context.getSharedPreferences(NameConstant.STAGE_LEVEL_LOCK_UNCLOCK_DATABASE, MODE_PRIVATE).edit();
        editor.putBoolean("stage" + id, false);
        editor.apply();
        if (levels != null && levels.size() > 0){
            levels.get(0).setLocked(false,context);
        }
    }

    public void updateLockedStateFromSharedPref(Context context){
        SharedPreferences pref = context.getSharedPreferences(NameConstant.STAGE_LEVEL_LOCK_UNCLOCK_DATABASE, MODE_PRIVATE);
        isLocked = (pref.getBoolean("stage" + id,true));
        if (levels != null){
            for (Level level : levels){
                level.updateLockedStateFromSharedPref(context);
            }
        }
    }




    public void setLocked(boolean locked,Context context) {
        isLocked = locked;
        SharedPreferences.Editor editor = context.getSharedPreferences(NameConstant.STAGE_LEVEL_LOCK_UNCLOCK_DATABASE, MODE_PRIVATE).edit();
        editor.putBoolean("stage" + id, locked);
        editor.apply();
    }

    public boolean isCompleted() {
        return isCompleted;
    }

    public void setCompleted(boolean completed, Context context) {
        isCompleted = completed;
        SharedPreferences.Editor editor = context.getSharedPreferences(NameConstant.STAGE_LEVEL_COMPLETED_DATABASE, MODE_PRIVATE).edit();
        editor.putBoolean("stage" + id, completed);
        editor.apply();
    }

    public void updateCompletedStateFromSharedPref(Context context){
        SharedPreferences pref = context.getSharedPreferences(NameConstant.STAGE_LEVEL_COMPLETED_DATABASE, MODE_PRIVATE);
        isCompleted = (pref.getBoolean("stage" + id,false));
        if (levels != null){
            for (Level level : levels){
                level.updateCompletedStateFromSharedPref(context);
            }
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeInt(stageNo);
        dest.writeInt(points);
        dest.writeString(image);
        dest.writeTypedList(levels);
        dest.writeByte((byte) (isLocked ? 1 : 0));
        dest.writeByte((byte) (isCompleted ? 1 : 0));
    }

    public Level getLastLevelUnlocked(){
        for (Level level : levels){
            if (!level.isLevelCompleted()){
                return level;
            }
        }
        return null;
    }
}
