
package com.blazestars.wordbaker.Modal;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import static android.content.Context.MODE_PRIVATE;

public class Level implements Parcelable {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("levelNo")
    @Expose
    private int levelNo;
    @SerializedName("points")
    @Expose
    private int points;
    @SerializedName("columns")
    @Expose
    private int columns;
    @SerializedName("rows")
    @Expose
    private int rows;
    @SerializedName("word1")
    @Expose
    private String word1;
    @SerializedName("word2")
    @Expose
    private String word2;
    @SerializedName("word3")
    @Expose
    private String word3;
    @SerializedName("word4")
    @Expose
    private String word4;
    @SerializedName("word5")
    @Expose
    private String word5;
    @SerializedName("wordHint1")
    @Expose
    private String wordHint1;
    @SerializedName("wordHint2")
    @Expose
    private String wordHint2;
    @SerializedName("wordHint3")
    @Expose
    private String wordHint3;
    @SerializedName("wordHint4")
    @Expose
    private String wordHint4;
    @SerializedName("wordHint5")
    @Expose
    private String wordHint5;
    @SerializedName("jumbledLetters")
    @Expose
    private String jumbledLetters;
    @SerializedName("wordPoint1")
    @Expose
    private int wordPoint1;
    @SerializedName("wordPoint2")
    @Expose
    private int wordPoint2;
    @SerializedName("wordPoint3")
    @Expose
    private int wordPoint3;
    @SerializedName("wordPoint4")
    @Expose
    private int wordPoint4;
    @SerializedName("wordPoint5")
    @Expose
    private int wordPoint5;
    @SerializedName("thumbnailImage")
    @Expose
    private String thumbnailImage;
    @SerializedName("fullImage")
    @Expose
    private String fullImage;

    @SerializedName("isLevelLocked")
    @Expose
    private boolean isLevelLocked;

    @SerializedName("isLevelCompleted")
    @Expose
    private boolean isLevelCompleted;

    protected Level(Parcel in) {
        id = in.readInt();
        name = in.readString();
        levelNo = in.readInt();
        points = in.readInt();
        columns = in.readInt();
        rows = in.readInt();
        word1 = in.readString();
        word2 = in.readString();
        word3 = in.readString();
        word4 = in.readString();
        word5 = in.readString();
        wordHint1 = in.readString();
        wordHint2 = in.readString();
        wordHint3 = in.readString();
        wordHint4 = in.readString();
        wordHint5 = in.readString();
        jumbledLetters = in.readString();
        wordPoint1 = in.readInt();
        wordPoint2 = in.readInt();
        wordPoint3 = in.readInt();
        wordPoint4 = in.readInt();
        wordPoint5 = in.readInt();
        thumbnailImage = in.readString();
        fullImage = in.readString();
        isLevelLocked = in.readByte() != 0;
        isLevelCompleted = in.readByte() != 0;
    }

    public static final Creator<Level> CREATOR = new Creator<Level>() {
        @Override
        public Level createFromParcel(Parcel in) {
            return new Level(in);
        }

        @Override
        public Level[] newArray(int size) {
            return new Level[size];
        }
    };

    public void setLocked(boolean locked, Context context) {
        isLevelLocked = locked;
        SharedPreferences.Editor editor = context.getSharedPreferences(NameConstant.STAGE_LEVEL_LOCK_UNCLOCK_DATABASE, MODE_PRIVATE).edit();
        editor.putBoolean("level" + id, locked);
        editor.apply();
    }

    public boolean isLocked() {
        return isLevelLocked;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevelNo() {
        return levelNo;
    }

    public void setLevelNo(int levelNo) {
        this.levelNo = levelNo;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getColumns() {
        return columns;
    }

    public void setColumns(int columns) {
        this.columns = columns;
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public String getWord1() {
        return word1;
    }

    public void setWord1(String word1) {
        this.word1 = word1;
    }

    public String getWord2() {
        return word2;
    }

    public void setWord2(String word2) {
        this.word2 = word2;
    }

    public String getWord3() {
        return word3;
    }

    public void setWord3(String word3) {
        this.word3 = word3;
    }

    public String getWord4() {
        return word4;
    }

    public void setWord4(String word4) {
        this.word4 = word4;
    }

    public String getWord5() {
        return word5;
    }

    public void setWord5(String word5) {
        this.word5 = word5;
    }

    public String getWordHint1() {
        return wordHint1;
    }

    public void setWordHint1(String wordHint1) {
        this.wordHint1 = wordHint1;
    }

    public String getWordHint2() {
        return wordHint2;
    }

    public void setWordHint2(String wordHint2) {
        this.wordHint2 = wordHint2;
    }

    public String getWordHint3() {
        return wordHint3;
    }

    public void setWordHint3(String wordHint3) {
        this.wordHint3 = wordHint3;
    }

    public String getWordHint4() {
        return wordHint4;
    }

    public void setWordHint4(String wordHint4) {
        this.wordHint4 = wordHint4;
    }

    public String getWordHint5() {
        return wordHint5;
    }

    public void setWordHint5(String wordHint5) {
        this.wordHint5 = wordHint5;
    }

    public String getJumbledLetters() {
        return jumbledLetters;
    }

    public void setJumbledLetters(String jumbledLetters) {
        this.jumbledLetters = jumbledLetters;
    }

    public int getWordPoint1() {
        return wordPoint1;
    }

    public void setWordPoint1(int wordPoint1) {
        this.wordPoint1 = wordPoint1;
    }

    public int getWordPoint2() {
        return wordPoint2;
    }

    public void setWordPoint2(int wordPoint2) {
        this.wordPoint2 = wordPoint2;
    }

    public int getWordPoint3() {
        return wordPoint3;
    }

    public void setWordPoint3(int wordPoint3) {
        this.wordPoint3 = wordPoint3;
    }

    public int getWordPoint4() {
        return wordPoint4;
    }

    public void setWordPoint4(int wordPoint4) {
        this.wordPoint4 = wordPoint4;
    }

    public int getWordPoint5() {
        return wordPoint5;
    }

    public void setWordPoint5(int wordPoint5) {
        this.wordPoint5 = wordPoint5;
    }

    public String getThumbnailImage() {
        return thumbnailImage;
    }

    public void setThumbnailImage(String thumbnailImage) {
        this.thumbnailImage = thumbnailImage;
    }

    public String getFullImage() {
        return fullImage;
    }

    public void setFullImage(String fullImage) {
        this.fullImage = fullImage;
    }

    public void updateLockedStateFromSharedPref(Context context) {
        SharedPreferences pref = context.getSharedPreferences(NameConstant.STAGE_LEVEL_LOCK_UNCLOCK_DATABASE, MODE_PRIVATE);
        isLevelLocked = pref.getBoolean("level" + id, true);
    }

    public boolean isLevelCompleted() {
        return isLevelCompleted;
    }

    public void setLevelCompleted(boolean levelCompleted, Context context) {
        isLevelCompleted = levelCompleted;
        SharedPreferences.Editor editor = context.getSharedPreferences(NameConstant.STAGE_LEVEL_COMPLETED_DATABASE, MODE_PRIVATE).edit();
        editor.putBoolean("level" + id, levelCompleted);
        editor.apply();
    }

    public void updateCompletedStateFromSharedPref(Context context) {
        SharedPreferences pref = context.getSharedPreferences(NameConstant.STAGE_LEVEL_COMPLETED_DATABASE, MODE_PRIVATE);
        isLevelCompleted = pref.getBoolean("level" + id, false);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeInt(levelNo);
        dest.writeInt(points);
        dest.writeInt(columns);
        dest.writeInt(rows);
        dest.writeString(word1);
        dest.writeString(word2);
        dest.writeString(word3);
        dest.writeString(word4);
        dest.writeString(word5);
        dest.writeString(wordHint1);
        dest.writeString(wordHint2);
        dest.writeString(wordHint3);
        dest.writeString(wordHint4);
        dest.writeString(wordHint5);
        dest.writeString(jumbledLetters);
        dest.writeInt(wordPoint1);
        dest.writeInt(wordPoint2);
        dest.writeInt(wordPoint3);
        dest.writeInt(wordPoint4);
        dest.writeInt(wordPoint5);
        dest.writeString(thumbnailImage);
        dest.writeString(fullImage);
        dest.writeByte((byte) (isLevelLocked ? 1 : 0));
        dest.writeByte((byte) (isLevelCompleted ? 1 : 0));
    }
}
