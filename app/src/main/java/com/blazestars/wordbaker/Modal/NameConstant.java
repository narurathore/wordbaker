package com.blazestars.wordbaker.Modal;

/**
 * Created by narayan on 1/8/15.
 */
public class NameConstant {

    public static final String GOOGLE_ANALYTICS_ID = "UA-105618279-1";
    public static final String BASE_URL = "http://blazestars.com/wordgame/api/";
    public static final String GET_GAME_DATA_URL = BASE_URL + "getGameData.php";
    public static final String HTTP_CLIENT_CACHE_DATABASE = "URL_CACHE";
    public static final String STAGE_LEVEL_LOCK_UNCLOCK_DATABASE = "stage_level_lock_unlock_database";
    public static final String STAGE_LEVEL_COMPLETED_DATABASE = "stage_level_COMPLETED_database";
    public static final String GAME_DATABASE = "game_data";
    public static final int WRONG_WORD_FORMED = 1;
    public static final int COREECT_WORD_FORMED = 2;
    public static final int WORD_ALREADY_FORMED = 3;
    public static final String INTENT_DATA_KEY_IS_DAILY_PUZZLE = "isDailyPuzzle";
    public static final String IS_SPIN_WHEEL_ENABLED = "is_spin_wheel_enabled";
    public static final String LAST_IS_SPIN_WHEEL_DISABLED = "last_is_spin_wheel_disabled";
    public static final int FACEBOOK_AD = 1;
    public static final int ADMOB_AD = 2;
}
